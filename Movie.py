# coding: utf-8

import datetime
import logging
import math

import Functions
import inspect
import Slack
import FunctionsCache
import FunctionsTmdb
import Settings
import omdb
import tmdbsimple as tmdb

tmdb.API_KEY = Settings.TMDB_API_KEY


class Movie:
    def __init__(self, title=None, year=0, original_title=None, lang=u'en', tmdb_id=0, imdb_id=None,
                 guess_id=True, fetch_data=True):

        # Save raw data
        self.forced_title = title
        self.forced_original_title = original_title
        self.lang = lang[:2].lower()
        self.forced_year = year

        # Convert year to integer
        if year is not None:
            self.forced_year = int(year)

        # TMDB
        self.tmdb_id = tmdb_id
        self.tmdb_title = None
        self.tmdb_original_title = None
        self.tmdb_year = 0
        self.tmdb_overview = None
        self.tmdb_poster_path = None
        self.tmdb_backdrop_path = None
        self.tmdb_tagline = None
        self.tmdb_runtime = 0

        # IMDB
        self.imdb_id = imdb_id

        # Scores
        self.score_rt_meter = None
        self.score_metacritic = None
        self.score_imdb = None
        self.score_tmdb = 0.0

        # Status boolean
        self.fetched_from_cache = False
        self.fetched_from_sources = False

        # Guess tmdb_id from name
        if not tmdb_id and guess_id:
            self.find_tmdb_id()

        # Fetch data of the movie (best source available)
        if self.tmdb_id and fetch_data:
            self.fetch_data()

    # ---- Look for tmdb ID from title, year and language (and save clean title, original, title and year)
    def find_tmdb_id(self):

        try:

            if not self.tmdb_id and self.imdb_id:
                self.tmdb_id = FunctionsCache.get_movie_tmdb_from_imdb(imdb_id=self.imdb_id)

            # Look in the cache
            if not self.tmdb_id and self.year and (self.title or self.original_title):
                self.tmdb_id = FunctionsCache.search_movie_id(title=self.title, original_title=self.original_title,
                                                        year=self.year, lang=self.lang)

            # Look in tmdb and add in cache if found for faster future lookup
            if not self.tmdb_id and self.year and (self.title or self.original_title):
                self.tmdb_id = FunctionsTmdb.search_movie_id(title=self.title, original_title=self.original_title, year=self.year,
                                                       lang=self.lang)
                FunctionsCache.add_movie_search_in_cache(tmdb_id=self.tmdb_id, year=self.year, lang=self.lang,
                                                   original_title=self.original_title, title=self.title)

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")


    # ---- Fetch data from cache then from tmdb
    def fetch_data(self):

        try:

            if self.tmdb_id and not self.fetched_from_cache:

                self.fetch_from_cache()

                # Check if an update is needed (cache older than a week)
                if self.fetched_from_cache:
                    diff = datetime.datetime.now() - self.cache_last_update

                    if diff.days > 7:
                        self.fetch_from_sources()
                        self.save_in_cache()

                else:
                    self.fetch_from_sources()
                    self.save_in_cache()

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")

    # ---- Fetch movie data from cache
    def fetch_from_cache(self):

        try:

            if self.tmdb_id and self.lang:

                result = FunctionsCache.fetch_movie_data(self.tmdb_id, self.lang)

                # If a result is found, save data
                if result:
                    self.imdb_id = result['imdb_id']
                    self.tmdb_title = result['title']
                    self.tmdb_original_title = result['original_title']
                    self.tmdb_year = result['year']
                    self.tmdb_overview = result['overview']
                    self.tmdb_tagline = result['tagline']
                    self.score_tmdb = result['vote']
                    self.score_rt_meter = result['score_rt_meter']
                    self.score_metacritic = result['score_metacritic']
                    self.score_imdb = result['score_imdb']
                    self.tmdb_poster_path = result['poster_path']
                    self.tmdb_backdrop_path = result['backdrop_path']
                    self.cache_last_update = result['last_update']
                    self.tmdb_runtime = result['runtime']
                    self.fetched_from_cache = True

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")

    # ---- Fetch movie data from sources
    def fetch_from_sources(self):

        try:

            if self.tmdb_id and self.lang:

                # Fetch from tmdb
                result_tmdb = FunctionsTmdb.fetch_movie_data(self.tmdb_id, self.lang)

                if result_tmdb:
                    self.imdb_id = result_tmdb['imdb_id']
                    self.tmdb_title = result_tmdb['title']
                    self.tmdb_original_title = result_tmdb['original_title']
                    if result_tmdb['year'] is not u'':
                        self.tmdb_year = result_tmdb['year']
                    self.tmdb_overview = result_tmdb['overview']
                    self.tmdb_tagline = result_tmdb['tagline']
                    self.score_tmdb = result_tmdb['vote']
                    self.tmdb_poster_path = result_tmdb['poster_path']
                    self.tmdb_backdrop_path = result_tmdb['backdrop_path']
                    self.tmdb_runtime = result_tmdb['runtime']
                    self.fetched_from_sources = True

                # If an imdb id is found fetch data from omdb
                if self.imdb_id:
                    result_omdb = omdb.imdbid(self.imdb_id, tomatoes=True)

                    if result_omdb:
                        self.score_rt_meter = result_omdb['tomato_meter']
                        self.score_metacritic = result_omdb['metascore']
                        self.score_imdb = result_omdb['imdb_rating']

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")


    # ----- Save movie in cache
    def save_in_cache(self):

        try:

            # If already in cache, update data
            if self.fetched_from_cache:
                FunctionsCache.update_movie(tmdb_id=self.tmdb_id, lang=self.lang, title=self.tmdb_title, \
                                            original_title=self.tmdb_original_title, vote=self.score_tmdb, \
                                            overview=self.tmdb_overview, poster_path=self.tmdb_poster_path, \
                                            backdrop_path=self.tmdb_backdrop_path, tagline=self.tmdb_tagline, \
                                            year=self.tmdb_year, imdb_id=self.imdb_id, score_rt_meter=self.score_rt_meter, \
                                            score_metacritic=self.score_metacritic, score_imdb=self.score_imdb,runtime=self.tmdb_runtime)

            # Otherwise create it
            elif self.fetched_from_sources:
                FunctionsCache.create_movie(tmdb_id=self.tmdb_id, lang=self.lang, title=self.tmdb_title,
                                            original_title=self.tmdb_original_title, vote=self.score_tmdb,
                                            overview=self.tmdb_overview, poster_path=self.tmdb_poster_path,
                                            backdrop_path=self.tmdb_backdrop_path, tagline=self.tmdb_tagline,
                                            year=self.tmdb_year, imdb_id=self.imdb_id, score_rt_meter=self.score_rt_meter,
                                            score_metacritic=self.score_metacritic, score_imdb=self.score_imdb,runtime=self.tmdb_runtime)

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")


    # ----- Force cache update
    def force_cache_update(self):

        try:

            if self.tmdb_id:
                self.fetch_from_sources()
                self.save_in_cache()

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")


    # ----- Get movie info in a json formatted string
    def get_json(self):

        try:

            if self.fetched_from_cache or self.fetched_from_sources:

                return {'tmdb_id': self.tmdb_id, 'lang': self.lang, 'title': self.tmdb_title,
                        'original_title': self.tmdb_original_title, 'vote_average': self.score_tmdb,
                        'overview': self.tmdb_overview,
                        'poster_path': self.tmdb_poster_path, 'backdrop_path': self.tmdb_backdrop_path,
                        'tagline': self.tmdb_tagline, 'year': self.tmdb_year, 'imdb_id': self.imdb_id,
                        'score_rt_meter': self.score_rt_meter, 'score_metacritic': self.score_metacritic,
                        'score_imdb': self.score_imdb, 'score_tmdb': self.score_tmdb, 'runtime':self.tmdb_runtime}

            else:
                return {'error': 'No movie found'}

        except Exception as e:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")

    # ---- Return the title
    @property
    def title(self):

        if self.tmdb_title:
            return self.tmdb_title

        elif self.forced_title:
            return self.forced_title

        else:
            return None

    # ---- Return the year
    @property
    def year(self):

        if self.tmdb_year:
            return int(self.tmdb_year)

        elif self.forced_year:
            return int(self.forced_year)

        else:
            return None

    # ---- Return the original title
    @property
    def original_title(self):

        if self.tmdb_original_title:
            return self.tmdb_original_title

        elif self.forced_original_title:
            return self.forced_original_title

        else:
            return None

    # ---- Return the clean original title
    @property
    def clean_original_title(self):

        if self.tmdb_original_title:
            return Functions.clean_string(self.tmdb_original_title)

        elif self.forced_original_title:
            return Functions.clean_string(self.forced_original_title)

        else:
            return None

    # ---- Return the clean original title
    @property
    def clean_original_title_plex(self):

        if self.tmdb_original_title:
            return Functions.clean_string(self.tmdb_original_title, 'plex')

        elif self.forced_original_title:
            return Functions.clean_string(self.forced_original_title, 'plex')

        else:
            return None

    # ---- Return the clean title
    @property
    def clean_title(self):

        if self.tmdb_title:
            return Functions.clean_string(self.tmdb_title)

        elif self.forced_title:
            return Functions.clean_string(self.forced_title)

        else:
            return None

    # ---- Return the clean title
    @property
    def clean_title_plex(self):

        if self.tmdb_title:
            return Functions.clean_string(self.tmdb_title, 'plex')

        elif self.forced_title:
            return Functions.clean_string(self.forced_title, 'plex')

        else:
            return None

    # ---- Return the tmdb ID
    @property
    def tmdb_id(self):

        if self.tmdb_id:
            return self.tmdb_id

        else:
            return None

    # ---- Return the imdb ID
    @property
    def imdb_id(self):

        if self.imdb_id:
            return self.imdb_id

        else:
            return None

    # ---- Return the overview
    @property
    def overview(self):

        if self.tmdb_overview:
            return self.tmdb_overview

        else:
            return None

    # ---- Print movie with clean formatting
    def __str__(self):

        if self.tmdb_id and self.title and self.original_title and self.year:
            return ('%s (%s) [%s] [%s]' % (self.title, self.year, self.tmdb_id, self.original_title)).encode('utf8')

        elif self.title and self.original_title and self.year:
            return ('%s (%s) [%s]' % (self.title, self.year, self.original_title)).encode('utf8')

        elif self.title and self.year:
            return ('%s (%s)' % (self.title, self.year)).encode('utf8')

        elif self.original_title and self.year:
            return ('%s (%s)' % (self.original_title, self.year)).encode('utf8')

        elif self.title:
            return ('%s' % self.title).encode('utf8')

        elif self.original_title:
            return ('%s' % self.original_title).encode('utf8')

        else:
            return ''


# Return the match confidence between two movies
def compare(movie1, movie2):

    try:

        match_confidence = 0

        # Try to fetch movies ID
        movie1.find_tmdb_id()
        movie2.find_tmdb_id()

        # Use tmdb IDs if they are defined for both movies
        if movie1.tmdb_id and movie2.tmdb_id:
            if str(movie1.tmdb_id) == str(movie2.tmdb_id):
                match_confidence = 1

        # Use imdb IDs if they are defined for both movies
        elif movie1.imdb_id and movie2.imdb_id:
            if str(movie1.imdb_id) == str(movie2.imdb_id):
                match_confidence = 1

        # Otherwise try to identify with titles
        else:

            # Fetch_data for the movies
            movie1.fetch_data()
            movie2.fetch_data()

            # Calculate score for every combination
            if movie1.title and movie2.title:
                match_confidence = Functions.compare_title(unicode(movie1.title), unicode(movie2.title))

            if movie1.original_title and movie2.original_title:
                match_confidence = max(match_confidence,
                                       Functions.compare_title(unicode(movie1.original_title), unicode(movie2.original_title)))

            if movie1.title and movie2.original_title:
                match_confidence = max(match_confidence, Functions.compare_title(unicode(movie1.title), unicode(movie2.original_title)) * 0.9)

            if movie2.title and movie1.original_title:
                match_confidence = max(match_confidence, Functions.compare_title(unicode(movie2.title), unicode(movie1.original_title)) * 0.9)

            # Apply year coeff
            match_confidence *= get_year_coeff(movie1.year, movie2.year)

        logging.info('Match confidence: {0:.0%}'.format(match_confidence))
        return match_confidence

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")


# Return the coefficient for match confidence for years
def get_year_coeff(year1, year2):

    try:

        if year1 and year2:

            if year1 == year2:
                return 1

            if math.fabs(year1 - year2) == 1:
                return 0.95

            if math.fabs(year1 - year2) == 2:
                return 0.8

            else:
                return 0.3

        else:
            return 0.5

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")
