# coding: utf-8
import difflib
import logging
import Settings
import requests
import tmdbsimple as tmdb

tmdb.API_KEY = Settings.TMDB_API_KEY

####################################### TMDB - Movies #######################################


# ----- Search for the tmdb ID on tmdb (to check if consistent +1 -1)
def search_movie_id(title=None, original_title=None, year=None, lang=None):

    # Search with title
    if title and year and lang:

        # Same year
        result = search_movie_id_on_tmdb(title=title, year=int(year), lang=lang)
        if result:
            return result

        # Year + 1
        result = search_movie_id_on_tmdb(title=title, year=int(year)+1, lang=lang)
        if result:
            return result

        # Year - 1
        result = search_movie_id_on_tmdb(title=title, year=int(year)-1, lang=lang)
        if result:
            return result

    # Search with original title
    if original_title and year:

        # Same year
        result = search_movie_id_on_tmdb(title=original_title, year=int(year))
        if result:
            return result

        # Year + 1
        result = search_movie_id_on_tmdb(title=original_title, year=int(year)+1)
        if result:
            return result

        # Year - 1
        result = search_movie_id_on_tmdb(title=original_title, year=int(year)-1)
        if result:
            return result

    if (not title and not year and not lang) or (not original_title and not year):
        logging.debug(u'Missing fields to look for tmdb ID')
        return None


# ------ Search for the movie id on tmdb for a single year
def search_movie_id_on_tmdb(title, year, lang=None):

    try:
        search = tmdb.Search()

        if lang:
            search.movie(query=title, year=year, language=lang)

        else:
            search.movie(query=title, year=year)

        if search.results:
            for result in search.results:
                original_title = ''
                if 'title' in result:
                    original_title = result['title']
                elif 'original_name' in result:
                    original_title = result['original_name']
                elif 'original_title' in result:
                    original_title = result['original_title']
                confidence = difflib.SequenceMatcher(a=title.lower(), b=original_title.lower()).ratio()
                if confidence > 0.9 or (len(search.results) == 1 and confidence > 0.8):
                    logging.debug(u'TMDB: Found ID [%s] for movie: %s (%s)' % (str(result[u'id']), title, year))
                    return str(result["id"])

        logging.error(u'FunctionsTmdb.search_movie_id_on_tmdb')
        return None

    except Exception as e:
        logging.error(u'FunctionsTmdb.search_movie_id_on_tmdb')
        return None


# ----- Get movie data from tmdb
def fetch_movie_data(tmdb_id, lang):

    if tmdb_id and lang:
        try:
            movie = tmdb.Movies(tmdb_id)
            response = movie.info(language=lang)

        except requests.exceptions.HTTPError:
            logging.debug(u'No movie for ID: [%s]' % tmdb_id)
            return None

        except:
            logging.error(u'FunctionsTmdb.fetch_movie_data')
            return None

        else:
            logging.debug(u'Found movie: %s' % response[u'title'])
            return {'tmdb_id': tmdb_id, 'title': response['title'],
                    'original_title': response['original_title'],
                    'vote': response['vote_average'], 'overview': response['overview'],
                    'poster_path': response['poster_path'], 'backdrop_path': response['backdrop_path'],
                    'tagline': response['tagline'], 'year': response['release_date'][:4],
                    'imdb_id': response['imdb_id'],'runtime': response['runtime']}


####################################### TMDB - TV Shows #######################################

# ----- Search for the tmdb ID on tmdb
def search_tvshow_id(title=None, original_title=None, lang=None):

    # Search tv show tmdb_id
    if title and lang:
        logging.debug(u'TMDB Calling search_tvshow_id_on_tmdb with title %s', title)
        result = search_tvshow_id_on_tmdb(title=title, lang=lang)
        if result:
            return result

    if original_title:
        logging.debug(u'TMDB Calling search_tvshow_id_on_tmdb with original_title %s', original_title)
        result = search_tvshow_id_on_tmdb(title=original_title)
        if result:
            return result

    if (not title and not lang) or (not original_title):
        logging.debug(u'TMDB TV Show not found!')
        return None


# ------ Search for TV Show id on tmdb
def search_tvshow_id_on_tmdb(title, lang=None):

    try:
        search = tmdb.Search()
        if lang:
            search.tv(query=title, language=lang)
        else:
            search.tv(query=title)

        if search.results:
            for result in search.results:
                confidence = difflib.SequenceMatcher(a=title.lower(), b=result['original_name'].lower()).ratio()
                if confidence > 0.9:
                    logging.debug(u'TMDB: Found ID [%s] for tv show: %s' % (str(result[u'id']), title))
                    return str(result["id"])

        logging.debug(u'TMDB: No ID found for tv-show: %s' % title)
        return None

    except:
        logging.error(u'FunctionsTmdb.search_tvshow_id_on_tmdb')
        return None


# ----- Get TV Show data from tmdb
def fetch_tvshow_data(tmdb_id, lang):

        if tmdb_id and lang:
            try:
                tvshow = tmdb.TV(tmdb_id)
                tvshow_info = tvshow.info(language=lang, append_to_response='external_ids')
                if tvshow_info:
                    return tvshow_info
            except requests.exceptions.HTTPError:
                logging.debug(u'No TV Show for ID: [%s]' % tmdb_id)
                return None

            except:
                logging.error(u'FunctionsTmdb.fetch_tvshow_data')
                return None


# ----- Get Season data from tmdb
def fetch_tvshow_season_data(tmdb_id, season_number, lang):

        if tmdb_id and lang:
            try:
                tvshow = tmdb.TV_Seasons(tmdb_id, season_number)
                response = tvshow.info(language=lang)
                return response
            except requests.exceptions.HTTPError:
                logging.debug(u'No TV Show for ID: [%s]' % tmdb_id)
                return None


# ----- Get Episode data from tmdb
def fetch_tvshow_episode_data(tmdb_id, season_number, episode_number, lang):

        if tmdb_id and lang:
            try:
                tvshow = tmdb.TV_Episodes(tmdb_id, season_number, episode_number)
                response = tvshow.info(language=lang)
                return response
            except requests.exceptions.HTTPError:
                logging.debug(u'No TV Show for ID: [%s]' % tmdb_id)
                return None

########################################### END ############################################