import unicodedata


# ----- Remove unnecessary keys from dict and add others
def update_dictionary(dic, media):
    del dic['year']
    del dic['length']
    del dic['origin']
    dic['currency'] = 'EUR'
    dic['is_adult'] = False
    dic['is_premium'] = False
    dic['country'] = 'FR'
    dic['lang'] = u'fr'
    dic['media_type'] = media

    return dic


# ----- Remove thrash info and reformat in nested list for easier use
def cleanse_movie_data(list):
    print list
    new_list = []
    for i in range(0, len(list), 6):
        print list[i]
        tmp_list = [list[i], list[i + 2], list[i + 4]]
        new_list.append(tmp_list)

    return new_list


# ----- Get rid of accented letters
def remove_accents(str):

    return ''.join(char for char in unicodedata.normalize('NFD', str)
                   if unicodedata.category(char) != 'Mn')


# ----- Get a XX hour [0]X minutes length format
def proper_len_format(dic):
    save = str(dic['length'][1])
    dic['length'] = str(dic['length'][0]) + 'h'
    if len(save) < 2:
        dic['length'] += '0'
    dic['length'] += save + 'min'

    return dic


# ----- Explicit
def get_max_index(spans):

    return spans[spans.index('Ma Recherche') + 1]


# ----- Removes duplicates in url list
def rm_url_dup(pages):
    new_list = []

    for elem in pages:
        if elem not in new_list:
            new_list.append(elem)

    return new_list
