# coding: utf-8


import HTMLParser
import codecs
import json
import logging
import Slack
import inspect
import Functions
import Settings
import sqlalchemy
import arrow
import TvShow
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()
engine = sqlalchemy.create_engine(URL(**Settings.DATABASE), pool_size=0, max_overflow=-1)


####################################### DB Connection #######################################

# ----- Connect to Postgresql database
def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    logging.debug('Connect to the Postgresql database')


# ----- Disconnect from Postgresql database
def db_disconnect():
    logging.debug('Disconnect from the Postgresql database')


def createSession():
    session = scoped_session(sessionmaker())
    session.remove()
    session.configure(bind=engine, autoflush=False, expire_on_commit=False)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    return session


def commit(session):
    session.commit()


####################################### TMDB - Movies #######################################

# ----- Search for tmdb ID in cache from imdb ID
def get_movie_tmdb_from_imdb(imdb_id):
    try:

        if imdb_id:
            result = engine.execute(
                "SELECT * FROM movies WHERE imdb_id='%s'" % (imdb_id))

            if result.rowcount > 0:
                tmdb_id = result.fetchone()['movie_id']
                return tmdb_id

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Search for tmdb ID in cache
def search_movie_id(year, title=None, original_title=None, lang=None):
    try:

        # Search first on original title + year
        if original_title:
            result = engine.execute(
                "SELECT * FROM search WHERE hash_search='%s'" % (Functions.hash_fct(original_title + str(year))))

        # Then search on title + language + year
        elif title and lang:
            result = engine.execute(
                "SELECT * FROM search WHERE hash_search='%s'" % (Functions.hash_fct(title + lang + str(year))))

        # Return result if found in the cache
        if result.rowcount > 0:
            tmdb_id = result.fetchone()['movie_id']
            logging.debug(u'Cache - Found ID [%s] for movie: %s' % (tmdb_id, title))
            return tmdb_id

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Add result of search in cache
def add_movie_search_in_cache(tmdb_id, year, lang=None, original_title=None, title=None):
    try:
        title = unicode(title)
        original_title = unicode(original_title)

        if tmdb_id and original_title:
            engine.execute("INSERT INTO search (hash_search, movie_id, title) VALUES ('%s', '%s', '%s')" % (
                Functions.hash_fct(original_title + str(year)), int(tmdb_id),
                Functions.clean_string(original_title) + ' (' + str(year) + ')'))
            logging.debug('Cache: Add search result to the cache')

        elif tmdb_id and title and lang:
            engine.execute("INSERT INTO search (hash_search, movie_id, title) VALUES ('%s', '%s', '%s')" % (
                Functions.hash_fct(title + lang + str(year)), int(tmdb_id),
                Functions.clean_string(title) + ' (' + lang + ') (' + str(year) + ')'))
            logging.debug('Cache: Add search result to the cache')

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Get movie data from cache
def fetch_movie_data(tmdb_id, lang):
    try:

        if tmdb_id and lang:

            result = engine.execute("SELECT * FROM movies WHERE movie_id=(%s) AND lang=(%s)", (tmdb_id, lang))
            movie = result.fetchone()

            if movie:
                logging.debug(u'Cache - Fetch data for: %s' % movie['title'])

                return {'title': movie['title'], 'original_title': movie['original_title'],
                        'vote': movie['vote'], 'overview': movie['overview'],
                        'poster_path': movie['poster_path'], 'backdrop_path': movie['backdrop_path'],
                        'tagline': movie['tagline'], 'year': movie['year'], 'imdb_id': movie['imdb_id'],
                        'score_rt_meter': movie['score_rt_meter'], 'score_metacritic': movie['score_metacritic'],
                        'score_imdb': movie['score_imdb'], 'last_update': movie['date_update'], 'runtime':movie['runtime']}

            else:
                return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create movie in cache
def create_movie(tmdb_id, lang, title, original_title, vote, overview, poster_path, backdrop_path, tagline, year,
                 imdb_id, score_rt_meter, score_metacritic, score_imdb, runtime):
    try:
        logging.debug(u'Cache - Create movie - %s' % title)
        engine.execute("INSERT INTO movies (movie_id, lang, title, vote, overview, poster_path, backdrop_path, tagline,\
        original_title, year, imdb_id, score_rt_meter, score_metacritic, score_imdb, runtime) VALUES ('%s', '%s', '%s', '%s',\
        '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (tmdb_id, Functions.format_sql(lang), Functions.format_sql(title), vote,
                          Functions.format_sql(overview),
                          Functions.format_sql(poster_path), Functions.format_sql(backdrop_path),
                          Functions.format_sql(tagline),
                          Functions.format_sql(original_title), year, Functions.format_sql(imdb_id), score_rt_meter,
                          score_metacritic, score_imdb, runtime))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Update movie in cache
def update_movie(tmdb_id, lang, title, original_title, vote, overview, poster_path, backdrop_path, tagline, year,
                 imdb_id, score_rt_meter, score_metacritic, score_imdb, runtime):
    try:
        logging.debug(u'Cache - Update movie - %s' % title)
        engine.execute("UPDATE movies SET title='%s', vote='%s', overview='%s', poster_path='%s', backdrop_path='%s',\
                        tagline='%s', original_title='%s', year='%s', imdb_id='%s', score_rt_meter='%s',\
                        score_metacritic='%s', score_imdb='%s', runtime='%s'  WHERE movie_id='%s' and lang='%s'"
                       % (Functions.format_sql(title), vote, Functions.format_sql(overview), poster_path, backdrop_path,
                          Functions.format_sql(tagline), Functions.format_sql(original_title), year, imdb_id,
                          score_rt_meter, score_metacritic, score_imdb, runtime,tmdb_id, lang))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### TMDB - TV Shows #######################################


# ----- Search for tmdb ID in cache from imdb ID
def get_tvshow_tmdb_from_imdb(imdb_id):
    logging.debug('Cache - get_tvshow_tmdb_from_imdb %s', imdb_id)
    try:
        if imdb_id:
            result = engine.execute(
                "SELECT tvshow_id FROM tv_shows WHERE imdb_id='%s'" % (imdb_id))

            if result.rowcount > 0:
                tmdb_id = result.fetchone()['tvshow_id']
                return tmdb_id

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Search for tmdb ID in cache
def search_tvshow_id(title=None, original_title=None, lang=None):
    logging.debug(u'Cache - search_tvshow_id - %s/%s', title, original_title)
    try:

        # Search first on original title + year
        if original_title:
            result = engine.execute(
                "SELECT * FROM search_tvshow WHERE hash_search_tvshow='%s'" % (Functions.hash_fct(original_title)))

        # Then search on title + language + year
        elif title and lang:
            result = engine.execute("SELECT * FROM search_tvshow WHERE hash_search_tvshow='%s'" % (Functions.hash_fct(title + lang)))

        # Return result if found in the cache
        if result.rowcount > 0:
            tmdb_id = result.fetchone()['movie_id']
            logging.debug(u'Cache - Found ID [%s] for tv show: %s' % (tmdb_id, title))
            return tmdb_id

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Add result of search in cache
def add_tvshow_search_in_cache(tmdb_id, lang=None, original_title=None, title=None):
    logging.debug(u'Cache - add_tvshow_search_in_cache - %s/%s', tmdb_id, original_title)
    try:
        if tmdb_id and original_title:
            engine.execute("INSERT INTO search_tvshow (hash_search_tvshow, movie_id, title) VALUES ('%s', '%s', '%s')" % (
                Functions.hash_fct(original_title), int(tmdb_id), original_title))
            logging.debug('Cache: Add search result to the cache')

        elif tmdb_id and title and lang:
            engine.execute("INSERT INTO search_tvshow (hash_search_tvshow, movie_id, title) VALUES ('%s', '%s', '%s')" % (
                Functions.hash_fct(title + lang), int(tmdb_id), Functions.format_sql(title + ' (' + lang + ')')))
            logging.debug('Cache: Add search result to the cache')

    except Exception as e:
        try:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + unicode(e.message,"utf-8"))
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + unicode(e.message,"utf-8"), "PyBot",
                           ":warning:")
        except:
            Slack.send_message("#errorpython", "Can't log the error because of encodage", "PyBot",
                           ":warning:")


# ----- Get tv show data from cache:
def fetch_tvshow_data(tmdb_id, lang):
    try:

        result = engine.execute("SELECT * FROM tv_shows WHERE tvshow_id=(%s) AND lang=(%s)", (tmdb_id, lang))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def fetch_tvshow_season_data(tmdb_id, lang, season):
    try:

        result = engine.execute(
            "SELECT * FROM tvshow_seasons WHERE serie_tmdb=(%s) AND lang=(%s) AND season_number=(%s)",
            (tmdb_id, lang, season))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def fetch_tvshow_seasons_data(tmdb_id, lang):
    try:

        result = engine.execute("SELECT * FROM tvshow_seasons WHERE serie_tmdb=(%s) AND lang=(%s)",
                                (tmdb_id, lang))
        resultset = []
        if result.rowcount > 0:
            for row in result:
                resultset.append(dict(row))
            return resultset

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def fetch_tvshow_seasons_number(tmdb_id, lang, is_special):
    try:

        result = engine.execute(
            "SELECT count(*) FROM tvshow_seasons WHERE serie_tmdb=(%s) AND lang=(%s) AND is_special=(%s) AND id!=0",
            (tmdb_id, lang, is_special))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def fetch_tvshow_episode_data(tmdb_id, lang, season, episode):
    try:

        result = engine.execute(
            "SELECT * FROM tvshow_episodes WHERE serie_tmdb=(%s) AND lang=(%s) AND season_number=(%s) AND episode_number=(%s)",
            (tmdb_id, lang, season, episode))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def fetch_tvshow_episodes_data(tmdb_id, lang, season):

    try:
        result = engine.execute(
            "SELECT * FROM tvshow_episodes WHERE serie_tmdb=(%s) AND lang=(%s) AND season_number=(%s)",
            (tmdb_id, lang, season))

        resultset = []
        if result.rowcount > 0:
            for row in result:
                resultset.append(dict(row))
            return resultset

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create tv show in cache
def create_tvshow(tmdb_id, lang, title, original_title, vote, overview, poster_path, backdrop_path, first_air_date,
                  imdb_id, score_rt_meter, score_metacritic, score_imdb, number_of_seasons):
    try:

        if first_air_date is None:
            first_air_date = "2099-12-31"

        logging.debug(u'Cache - Create TV show - %s' % title)
        engine.execute("INSERT INTO tv_shows (tvshow_id, lang, title, vote, overview, poster_path, backdrop_path,\
        original_title, first_air_date, imdb_id, score_rt_meter, score_metacritic, score_imdb, number_of_seasons) "
                       "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (tmdb_id,
                          Functions.format_sql(lang),
                          Functions.format_sql(title),
                          vote,
                          Functions.format_sql(overview),
                          Functions.format_sql(poster_path),
                          Functions.format_sql(backdrop_path),
                          Functions.format_sql(original_title),
                          first_air_date,
                          Functions.format_sql(imdb_id),
                          score_rt_meter,
                          score_metacritic,
                          score_imdb,
                          number_of_seasons))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + unicode(e.message,"utf-8"))
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + unicode(e.message,"utf-8"), "PyBot",
                           ":warning:")


# ----- Update TV Show in cache
def update_tvshow(tmdb_id, lang, title, original_title, vote, overview, poster_path, backdrop_path, first_air_date,
                  imdb_id, score_rt_meter, score_metacritic, score_imdb, number_of_seasons):
    try:
        logging.debug(u'Cache - Update TV show - %s' % title)
        engine.execute("UPDATE tv_shows SET title='%s', vote='%s', overview='%s', poster_path='%s', backdrop_path='%s',\
                        original_title='%s', first_air_date='%s', imdb_id='%s', score_rt_meter='%s',\
                        score_metacritic='%s', score_imdb='%s', number_of_seasons='%s' WHERE tvshow_id='%s' and lang='%s'"
                       % (Functions.format_sql(title), vote, Functions.format_sql(overview), poster_path, backdrop_path,
                          Functions.format_sql(original_title), first_air_date, imdb_id,
                          score_rt_meter, score_metacritic, score_imdb, number_of_seasons, tmdb_id, lang))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def create_season(tmdb_id, lang, season, poster_path, id, episode_count, air_date, session):
    logging.debug(u'Cache - Create TV show season - %s season %d', tmdb_id, season)

    try:

        if season <= 0:
            is_special = True
        else:
            is_special = False

        if air_date is None:
            air_date = '2099-12-31'

        if air_date is None:
            air_date = '2099-12-31'

        session.execute(
            "INSERT INTO tvshow_seasons (serie_tmdb, lang, season_number, poster_path, id, episode_count, air_date, "
            " is_special) "
            "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
            % (tmdb_id,
               Functions.format_sql(lang),
               season,
               Functions.format_sql(poster_path),
               id,
               episode_count,
               air_date,
               is_special))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_season(tmdb_id, lang, season, poster_path, id, episode_count, air_date, session):
    logging.debug(u'Cache - Update TV show season - %s season %d', tmdb_id, season)

    try:
        if season <= 0:
            is_special = True
        else:
            is_special = False

        if air_date is None:
            air_date = '2099-12-31'

        session.execute("UPDATE tvshow_seasons SET poster_path='%s', id='%s', episode_count='%s', "
                       "air_date='%s', is_special='%s' WHERE serie_tmdb='%s' "
                       "and lang='%s' and season_number='%s'"
                       % (Functions.format_sql(poster_path),
                          id,
                          episode_count,
                          air_date,
                          is_special,
                          tmdb_id, lang, season))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def create_episode(serie_tmdb, name, lang, season_number, still_path, episode_number, id, air_date, overview,
                   vote_average, session):
    try:
        logging.debug(u'Cache - Create TV show episode- Season %d Episode %d', season_number, episode_number)

        if air_date is None:
            air_date = '2099-12-31'

        session.execute(
            "INSERT INTO tvshow_episodes (serie_tmdb, lang, season_number, episode_number, name, still_path, id, "
            "air_date, overview, vote_average) "
            "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
            % (serie_tmdb,
               Functions.format_sql(lang),
               season_number,
               episode_number,
               Functions.format_sql(name),
               Functions.format_sql(still_path),
               id,
               air_date,
               Functions.format_sql(overview),
               vote_average
               ))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_episode(serie_tmdb, name, lang, season_number, still_path, episode_number, id, air_date, overview,
                   vote_average, session):
    logging.debug(u'Cache - Update TV show episode - %s season %d episodes %d', name, season_number, episode_number)

    try:

        if air_date is None:
            air_date = '2099-12-31'

            session.execute("UPDATE tvshow_episodes SET still_path='%s', id='%s', "
                       "air_date='%s', overview='%s', name='%s', vote_average='%s' WHERE serie_tmdb='%s' "
                       "and lang='%s' and season_number='%s' and episode_number='%s'"
                       % (Functions.format_sql(still_path),
                          id,
                          air_date,
                          Functions.format_sql(overview),
                          Functions.format_sql(name),
                          vote_average,
                          serie_tmdb, lang, season_number, episode_number))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### iTunes Movie #######################################

# ----- Search for iTunes release
def search_release_itunes(tmdb_id, country, lang):
    try:
        result = engine.execute(
            "SELECT * FROM release_itunes WHERE tmdb_id=(%s) AND country=(%s) AND lang=(%s) ",
            (tmdb_id, country, lang))

        # Return result if found in the cache
        if result.rowcount > 0:
            logging.debug(u'Cache - Found iTunes release for movie: [%s]' % (tmdb_id))
            first_result = result.fetchone()
            return {'url': first_result['url'], 'price_sd': first_result['price_sd'],
                    'price_hd': first_result['price_hd'],
                    'rent_sd': first_result['rent_sd'], 'rent_hd': first_result['rent_hd'],
                    'currency': first_result['currency'], 'last_update': first_result['date_update'],
                    'preorder': first_result['preorder'], 'track_id': first_result['track_id'],
                    'date_creation': first_result['date_creation']}

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create iTunes release in cache
def create_release_itunes(tmdb_id, country, lang, preorder, url, price_sd, price_hd, currency, rent_sd, rent_hd,
                          track_id):
    logging.debug(u'Cache - Create iTunes release - [%s]' % tmdb_id)
    try:

        engine.execute("INSERT INTO release_itunes (tmdb_id, country, lang, preorder, url, price_sd, price_hd, rent_sd, \
        rent_hd, currency, track_id) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (
                       tmdb_id, country.lower(), lang, preorder, Functions.format_sql(url), price_sd, price_hd, rent_sd,
                       rent_hd, currency, track_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Update iTunes release in cache
def update_release_itunes(tmdb_id, country, lang, preorder, url, price_sd, price_hd, currency, rent_sd, rent_hd,
                          track_id):
    logging.debug(u'Cache - Update iTunes release - [%s]' % tmdb_id)

    try:

        engine.execute("UPDATE release_itunes SET preorder='%s', url='%s', price_sd='%s', price_hd='%s', rent_sd='%s', \
        rent_hd='%s', currency='%s', track_id='%s' WHERE tmdb_id='%s' and lang='%s' and country='%s' "
                       % (preorder, url, price_sd, price_hd, rent_sd, rent_hd, currency, track_id, tmdb_id, lang,
                          country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### iTunes TvShow #######################################

# ----- Get tv show data from cache:
def search_tvshow_itunes(tmdb_id, lang, country):
    try:

        result = engine.execute("SELECT * FROM tvshows_itunes "
                                "WHERE tmdb_id=(%s) AND country=(%s)", (tmdb_id, country.lower()))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_season_itunes(tmdb_id, lang, season, country):
    try:

        result = engine.execute("SELECT * FROM tvshows_itunes_seasons "
                                "WHERE tmdb_id=(%s) AND season=(%s) AND country=(%s)",
                                (tmdb_id, season, country))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_episode_itunes(tmdb_id, lang, season, episode, country):
    try:

        result = engine.execute(
            "SELECT * FROM tvshows_itunes_episodes "
            "WHERE tmdb_id=(%s) AND season=(%s) AND episode=(%s) AND country=(%s)",
            (tmdb_id, season, episode, country))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_tvshow_itunes(tmdb_id, country, lang, title):
    logging.debug(u'Cache - Update Itunes TV show - [%s]', tmdb_id)

    try:

        engine.execute(
            "UPDATE tvshows_itunes SET country='%s', title='%s'"
            " WHERE tmdb_id='%s' AND lang='%s'"
            % (
                country.lower(), Functions.format_sql(title), tmdb_id, lang))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def create_tvshow_itunes(tmdb_id, country, lang, title):
    logging.debug(u'Cache - Create Itunes TV Show - [%s]', tmdb_id)

    try:

        engine.execute("INSERT INTO tvshows_itunes (tmdb_id, country, lang, title) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, Functions.format_sql(title)))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_itunes_season(tmdb_id, country, lang, season, overview, title, url, price_season_hd, price_season_sd,
                         rent_season_hd, rent_season_sd, preorder_season_hd, preorder_season_sd, episode_count,
                         currency, first_air_date, is_preorder, collection_id):
    try:

        engine.execute(
            "UPDATE tvshows_itunes_seasons SET title='%s', overview='%s', url='%s', "
            "currency='%s', price_season_hd='%s', price_season_sd='%s', rent_season_hd='%s', rent_season_sd='%s', "
            "preorder_season_hd='%s', preorder_season_sd='%s', is_preorder=(%s), collection_id=(%s)"
            "WHERE tmdb_id='%s' AND season='%s' AND lang='%s' AND country='%s'"
            % (
                Functions.format_sql(title), Functions.format_sql(overview), Functions.format_sql(url), currency,
                price_season_hd, price_season_sd,
                rent_season_hd, rent_season_sd,
                preorder_season_hd, preorder_season_sd, is_preorder, collection_id,
                tmdb_id, season, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_itunes_season(tmdb_id, country, lang, season, overview, title, url, price_season_hd, price_season_sd,
                      rent_season_hd, rent_season_sd, preorder_season_hd, preorder_season_sd, episode_count, currency,
                      first_air_date, is_preorder, collection_id):
    logging.debug(u'Cache - Create Itunes TV Show Season [%s - season %s]', tmdb_id, season)

    try:

        engine.execute("INSERT INTO tvshows_itunes_seasons (tmdb_id, country, lang, season, "
                       "overview, title, url, price_season_hd, price_season_sd, "
                       "rent_season_hd, rent_season_sd, preorder_season_hd, "
                       "preorder_season_sd, episode_count, currency, first_air_date, is_preorder, collection_id) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'"
                       ", '%s', '%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, season, Functions.format_sql(overview),
                           Functions.format_sql(title), Functions.format_sql(url),
                           price_season_hd, price_season_sd, rent_season_hd, rent_season_sd,
                           preorder_season_hd, preorder_season_sd, episode_count, currency, first_air_date, is_preorder,
                           collection_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_itunes_episode(tmdb_id, country, lang, season, episode, overview, title, url, currency, price_episode_hd,
                       price_episode_sd, rent_episode_hd, rent_episode_sd, preorder_episode_hd, preorder_episode_sd,
                       is_preorder, is_season_only, is_free):
    logging.debug(u'Cache - Create Itunes TV Show Episode- [%s - season %s ep.%s]', tmdb_id, season, episode)

    try:

        engine.execute("INSERT INTO tvshows_itunes_episodes (tmdb_id, country, lang, season, episode, "
                       "overview, title, url, currency, price_episode_hd, price_episode_sd, "
                       "rent_episode_hd, rent_episode_sd, preorder_episode_hd, "
                       "preorder_episode_sd, is_preorder, is_season_only, is_free) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'"
                       ", '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, season, episode,
                           Functions.format_sql(overview), Functions.format_sql(title), Functions.format_sql(url),
                           currency, price_episode_hd, price_episode_sd, rent_episode_hd, rent_episode_sd,
                           preorder_episode_hd, preorder_episode_sd, is_preorder, is_season_only, is_free))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_itunes_episode(tmdb_id, country, lang, season, episode, overview, title, url, currency, price_episode_hd,
                          price_episode_sd, rent_episode_hd, rent_episode_sd, preorder_episode_hd, preorder_episode_sd,
                          is_preorder, is_season_only, is_free):
    logging.debug(u'Cache - Update Itunes TV Show Episode- [%s - season %s ep.%s]', tmdb_id, season, episode)

    try:

        engine.execute(
            "UPDATE tvshows_itunes_episodes SET title='%s', overview='%s', url='%s', "
            "currency='%s',"
            "price_episode_hd='%s', price_episode_sd='%s', rent_episode_hd='%s', rent_episode_sd='%s', "
            "preorder_episode_hd='%s', preorder_episode_sd='%s', is_preorder=(%s), is_season_only=(%s), is_free=(%s)"
            "WHERE tmdb_id='%s' AND season='%s' AND episode='%s' AND lang='%s' AND country='%s'"
            % (
                Functions.format_sql(title), Functions.format_sql(overview), Functions.format_sql(url), currency,
                price_episode_hd, price_episode_sd,
                rent_episode_hd, rent_episode_sd,
                preorder_episode_hd, preorder_episode_sd, is_preorder, is_season_only, is_free,
                tmdb_id, season, episode, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Netflix Movies#######################################

# ----- Search for Netflix release
def search_release_netflix(country, netflix_id=None, tmdb_id=None):
    try:

        # Search in cache with netflix ID
        if netflix_id:
            result = engine.execute("SELECT * FROM release_netflix WHERE netflix_id=(%s) AND country=(%s)",
                                    (netflix_id, country.lower()))

        # Search in cache with tmdb ID
        elif tmdb_id:
            result = engine.execute("SELECT * FROM release_netflix WHERE tmdb_id=(%s) AND country=(%s)",
                                    (tmdb_id, country.lower()))

        # Return result if found in the cache
        if result.rowcount > 0:
            logging.debug(u'Cache - Found Netflix release for movie: [%s]' % (tmdb_id))
            first_result = result.fetchone()
            return {'date_added': first_result['date_added'], 'date_removed': first_result['date_removed'],
                    'available': first_result['available'], 'netflix_id': first_result['netflix_id'],
                    'tmdb_id': first_result['tmdb_id'], 'date_update': first_result['date_update']}

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create Netflix release in cache
def create_release_netflix(tmdb_id, country, date_added, date_removed, available, netflix_id):
    logging.debug(u'Cache - Create netflix release - [%s]' % tmdb_id)

    try:

        engine.execute("INSERT INTO release_netflix (tmdb_id, country, date_added, date_removed, available, netflix_id)\
                       VALUES ('%s', '%s', '%s', '%s', '%s', '%s')"
                       % (tmdb_id, country.lower(), date_added, date_removed, available, netflix_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Update Netflix release in cache
def update_release_netflix(tmdb_id, country, date_added, date_removed, available, netflix_id):
    logging.debug(u'Cache - Update netflix release - [%s]' % netflix_id)

    try:
        engine.execute("UPDATE release_netflix SET date_added='%s', date_removed='%s', available='%s', netflix_id='%s'\
                        WHERE tmdb_id='%s' and country='%s' "
                       % (date_added, date_removed, available, netflix_id, tmdb_id, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Mark Netflix release as closed in cache
def close_release_netflix(netflix_id, country, date_removed, available):
    logging.debug(u'Cache - Update netflix release - [%s]' % netflix_id)

    try:

        engine.execute(
            "UPDATE release_netflix SET date_removed='%s', available='%s' WHERE netflix_id='%s' and country='%s'"
            % (date_removed, available, netflix_id, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Get netflix country code
def get_country_code_netflix(country):
    try:

        result = engine.execute("SELECT * FROM country_list_netflix WHERE country=(%s)",
                                (country.lower()))
        if result.rowcount > 0:
            first_result = result.fetchone()
            return {"country": first_result["country"], "code": first_result["code"]}
        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Save netflix country code
def create_country_code_netflix(country, code):
    logging.debug(u'Cache - Create netflix country code - [%s]' % country)

    try:

        engine.execute("INSERT INTO country_list_netflix (country,code)\
                       VALUES ('%s', '%s')"
                       % (country.lower(), code))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Get recently on Netflix
def get_recently_netflix(country, page):
    try:

        skip = page * 20

        results = engine.execute(
            "SELECT * FROM release_netflix WHERE country=(%s) AND date_added!='2099-12-31' ORDER BY date_added DESC LIMIT 20 OFFSET %s",
            (country.lower(), skip))

        movies = []
        for movie in results:
            movies.append(movie[0])

        return movies

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Netflix TvShows#######################################


# ----- Get tv show data from cache:
def search_tvshow_netflix(tmdb_id, lang, country):
    try:
        result = engine.execute("SELECT * FROM tvshows_netflix "
                                "WHERE tmdb_id=(%s) AND lang=(%s) AND country=(%s)", (tmdb_id, lang, country))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_season_netflix(tmdb_id, lang, season_number, country):
    try:
        result = engine.execute("SELECT * FROM tvshows_netflix_seasons "
                                "WHERE tmdb_id=(%s) AND lang=(%s) AND season_number=(%s) AND country=(%s)",
                                (tmdb_id, lang, season_number, country))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_episode_netflix(tmdb_id, lang, season_number, episode_number, country):
    try:
        result = engine.execute(
            "SELECT * FROM tvshows_netflix_episodes "
            "WHERE tmdb_id=(%s) AND lang=(%s) AND season_number=(%s) AND episode_number=(%s)",
            (tmdb_id, lang, season_number, episode_number))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def netflix_clear(netflix_id, session):
    try:
        session.execute("DELETE FROM tvshows_netflix WHERE netflix_id='%s'" % (netflix_id))
        session.execute("DELETE FROM tvshows_netflix_seasons WHERE netflix_id='%s'" % (netflix_id))
        session.execute("DELETE FROM tvshows_netflix_episodes WHERE serie_netflix_id='%s'" % (netflix_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_tvshow_netflix(tmdb_id, country, lang, title, netflix_id, date_added, date_removed, rate_netflix,
                          seasons_number, url, session):
    logging.debug(u'Cache - Update netflix TV show - [%s]', tmdb_id)

    try:
        session.execute(
            "UPDATE tvshows_netflix SET country='%s', title='%s', netflix_id='%s', date_added='%s', date_removed='%s', "
            "rate_netflix='%s', seasons_number='%s', url='%s'"
            " WHERE tmdb_id='%s' AND lang='%s' AND country='%s"
            % (
                country.lower(), Functions.format_sql(title), netflix_id, date_added, date_removed, rate_netflix,
                seasons_number, url, tmdb_id, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def create_tvshow_netflix(tmdb_id, country, lang, title, netflix_id, date_added, date_removed, rate_netflix,
                          seasons_number, url, session):
    logging.debug(u'Cache - Create netflix TV Show - [%s]', tmdb_id)

    try:
        session.execute(
            "INSERT INTO tvshows_netflix (tmdb_id, country, lang, title, netflix_id, date_added, date_removed,"
            "rate_netflix, seasons_number, url) "
            "VALUES "
            "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
            % (
                tmdb_id, country.lower(), lang, Functions.format_sql(title), netflix_id, date_added,
                date_removed, rate_netflix, seasons_number, url))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_netflix_season(tmdb_id, country, lang, season_number, episode_count, available, session, date_added,
                          date_removed):
    try:
        session.execute(
            "UPDATE tvshows_netflix_seasons SET episode_count='%s', available='%s', date_added='%s, date_removed='%s"
            "WHERE tmdb_id='%s' AND season_number='%s' AND lang='%s' AND country='%s'"
            % (
                episode_count, available, date_added, date_removed,
                tmdb_id, season_number, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_netflix_season(tmdb_id, country, lang, season_number, episode_count, available, session, netflix_id, date_added,
                       date_removed):
    logging.debug(u'Cache - Create netflix TV Show Season [%s - season %s]', tmdb_id, season_number)

    try:
        session.execute(
            "INSERT INTO tvshows_netflix_seasons (tmdb_id, country, lang, season_number, episode_count, available, netflix_id, date_added, date_removed) "
            "VALUES "
            "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
            % (
                tmdb_id, country.lower(), lang, season_number, episode_count, available, netflix_id, date_added,
                date_removed))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_netflix_episode(tmdb_id, lang, season_number, episode_number, country, netflix_id, url, available, session,
                        date_added, date_removed, serie_netflix_id):
    logging.debug(u'Cache - Create netflix TV Show Episode- [%s - season %s ep.%s]', tmdb_id, season_number,
                  episode_number)

    try:
        session.execute("INSERT INTO tvshows_netflix_episodes (tmdb_id, country, lang, season_number, episode_number, "
                        "netflix_id, url, available, date_added, date_removed, serie_netflix_id) "
                        "VALUES "
                        "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                        % (
                            tmdb_id, country.lower(), lang, season_number, episode_number, netflix_id, url, available,
                            date_added, date_removed, serie_netflix_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Mark Netflix release as closed in cache
def close_tvshow_netflix(netflix_id, country, date_removed, available):
    logging.debug(u'Cache - Close Netflix TvShow - [%s]' % netflix_id)

    try:
        engine.execute(
            "UPDATE tvshows_netflix SET date_removed='%s', available='%s' WHERE netflix_id='%s' and country='%s'"
            % (date_removed, available, netflix_id, country.lower()))
        engine.execute(
            "UPDATE tvshows_netflix_seasons SET date_removed='%s', available='%s' WHERE netflix_id='%s' and country='%s'"
            % (date_removed, available, netflix_id, country.lower()))
        engine.execute(
            "UPDATE tvshows_netflix_episodes SET date_removed='%s', available='%s' WHERE netflix_id='%s' and country='%s'"
            % (date_removed, available, netflix_id, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Cinema #######################################


# ----- Search for cinema release
def search_release_cinema(tmdb_id, country):
    try:

        result = engine.execute(
            "SELECT * FROM release_cinema WHERE tmdb_id=(%s) AND country=(%s)",
            (tmdb_id, country))

        # Return result if found in the cache
        if result.rowcount > 0:
            logging.debug(u'Cache - Found cinema release for movie: [%s]' % (tmdb_id))
            first_result = result.fetchone()
            return {'release_date': first_result['release_date'], 'last_update': first_result['date_update']}

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create/update cinema release in cache
def create_release_cinema(tmdb_id, country, release_date):
    query = "insert into release_cinema(tmdb_id, country, release_date) values('%s', '%s', '%s')" \
            % (tmdb_id, country.lower(), release_date)

    try:
        engine.execute(query)
        logging.debug(u'Cache - Create cinema release - [%s]' % tmdb_id)

    except Exception as e:

        if hasattr(e, "orig"):
            if e.orig.pgcode == "23505":
                query = "update release_cinema set release_date='%s' WHERE tmdb_id='%s' and country='%s';" \
                        % (release_date, tmdb_id, country.lower())

                engine.execute(query)
                logging.debug(u'Cache - Update cinema release - [%s]' % tmdb_id)

            else:
                logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
                Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message,
                                   "PyBot", ":warning:")


# ----- Create empty cinema release in cache
def create_release_cinema_empty(tmdb_id, country):
    query = "insert into release_cinema(tmdb_id, country, release_date) values('%s', '%s', NULL)" \
            % (tmdb_id, country.lower())
    try:
        engine.execute(query)
        logging.debug(u'Cache - Create empty cinema release - [%s]' % tmdb_id)
    except Exception as e:
        if hasattr(e, "orig"):
            if e.orig.pgcode == "23505":
                query = "update release_cinema set release_date=NULL WHERE tmdb_id='%s' and country='%s';" \
                        % (tmdb_id, country.lower())

                engine.execute(query)
                logging.debug(u'Cache - Update empty cinema release - [%s]' % tmdb_id)

        else:
            logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
            Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message,
                               "PyBot", ":warning:")


# ----- Get recently in cinema
def get_recently_cinema(country):
    try:

        lower_bound = arrow.now().replace(weeks=-1).format('YYYY-MM-DD')
        upper_bound = arrow.now().replace(weeks=+1).format('YYYY-MM-DD')

        results = engine.execute(
            "SELECT * FROM release_cinema WHERE country='%s' AND release_date>='%s' AND release_date<'%s'"
            % (country.lower(), lower_bound, upper_bound))

        movies = []
        for movie in results:
            movies.append(movie[0])

        return movies

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Amazon Movie #######################################

# get amazon instant video availibility for the country
def get_amazon_supported_countries(country):
    try:

        return engine.execute("SELECT * FROM country_list_amazon WHERE country='%s'" % (country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Search for Amazon release
def search_release_amazon(tmdb_id, country):
    try:

        result = engine.execute(
            "SELECT * FROM release_amazon WHERE tmdb_id=(%s) AND country=(%s)",
            (tmdb_id, country.lower()))

        # Return result if found in the cache
        if result.rowcount > 0:
            logging.debug(u'Cache - Found Amazon release for movie: [%s]' % tmdb_id)
            first_result = result.fetchone()
            return {
                'url': first_result['url'],
                'price': first_result['lowest_price'],
                'currency': first_result['currency'],
                'amazon_id': first_result['amazon_id'],
                'date_update': first_result['date_update'],
                'date_creation': first_result['date_creation'],
                'is_prime': first_result['is_prime'],
                'is_preorder': first_result['is_preorder'],
                'is_adult': first_result['is_adult'],
                'title': first_result['title'],
                'product_group': first_result['product_group'],
                'price_hd': first_result['price_hd'],
                'price_sd': first_result['price_sd'],
                'rent_hd': first_result['rent_hd'],
                'rent_sd': first_result['rent_sd'],
                'preorder_hd': first_result['preorder_hd'],
                'preorder_sd': first_result['preorder_sd'],
                'package': first_result['package'],
            }

        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create Amazon release in cache
def create_release_amazon(tmdb_id, country, lang, title, url, price, currency, amazon_id, date_update, date_creation,
                          is_prime, is_adult, is_preorder, product_group, product_type, price_hd, price_sd,
                          rent_hd, rent_sd, preorder_hd, preorder_sd, package):
    try:

        logging.debug(u'Cache - Create Amazon release - [%s]' % tmdb_id)

        engine.execute("INSERT INTO release_amazon (tmdb_id, country, lang, title, url, lowest_price, "
                       "currency, amazon_id, date_creation, date_update, is_prime, is_adult, is_preorder, product_group, "
                       "product_type, price_hd, price_sd, rent_hd, rent_sd, preorder_hd, preorder_sd, package) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'"
                       ", '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, Functions.format_sql(title), Functions.format_sql(url),
                           price,
                           currency, amazon_id, date_update, date_creation, is_prime, is_adult, is_preorder,
                           product_group,
                           product_type, price_hd, price_sd, rent_hd, rent_sd, preorder_hd,
                           preorder_sd, package))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Update Amazon release in cache
def update_release_amazon(tmdb_id, country, url, price, currency, amazon_id, date_update, date_creation, price_hd,
                          price_sd, rent_hd, rent_sd, preorder_hd, preorder_sd, is_preorder, lang, is_prime, package):
    logging.debug(u'Cache - Update Amazon release - [%s]' % tmdb_id)

    try:
        engine.execute(
            "UPDATE release_amazon SET url='%s', lowest_price='%s', currency='%s', amazon_id='%s', date_creation='%s', "
            "date_update='%s', price_hd='%s', price_sd='%s', rent_hd='%s', rent_sd='%s', "
            "preorder_hd='%s', preorder_sd='%s' , is_preorder='%s', lang='%s', is_prime='%s', package='%s' WHERE tmdb_id='%s' and country='%s'"
            % (
                Functions.format_sql(url), price, currency, amazon_id, date_creation, date_update, price_hd, price_sd,
                rent_hd, rent_sd, preorder_hd, preorder_sd, is_preorder, lang, is_prime, package, tmdb_id, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")

####################################### Amazon TvShow #######################################

# ----- Search for Amazon TV Show
def search_tvshow_amazon(tmdb_id, country, lang):
    try:

        result = engine.execute(
            "SELECT * FROM tvshows_amazon "
            "WHERE "
            "tmdb_id=(%s) AND country=(%s) AND lang=(%s)",
            (tmdb_id, country.lower(), lang))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create Amazon TV show in cache
def create_tvshow_amazon(tmdb_id, country, lang, title, url, product_group, product_type):
    logging.debug(u'Cache - Create Amazon TV Show - [%s]' % tmdb_id)

    try:

        engine.execute("INSERT INTO tvshows_amazon (tmdb_id, country, lang, title, url,  product_group, product_type) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, Functions.format_sql(title), Functions.format_sql(url),
                           product_group, product_type))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Create Amazon TV show in cache
def update_tvshow_amazon(tmdb_id, country, lang, title, url, product_group, product_type):
    logging.debug(u'Cache - Update Amazon TV show - [%s]' % tmdb_id)

    try:

        engine.execute(
            "UPDATE tvshows_amazon SET title='%s', url='%s', product_group='%s', product_type='%s'"
            " WHERE tmdb_id='%s' AND lang='%s' AND country='%s'"
            % (
                Functions.format_sql(title), Functions.format_sql(url), product_group, product_type,
                tmdb_id, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Search for Amazon Season
def search_tvshow_season_amazon(tmdb_id, country, lang, season):
    try:

        result = engine.execute(
            "SELECT * FROM tvshows_amazon_seasons "
            "WHERE "
            "tmdb_id=(%s) AND country=(%s) AND lang=(%s) AND season=(%s)",
            (tmdb_id, country.lower(), lang, season))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_amazon_season(tmdb_id, country, lang, season_amazon_id, season, overview, title, url, price_season_hd,
                      price_season_sd, rent_season_hd, rent_season_sd, preorder_season_hd, preorder_season_sd,
                      episode_count, currency, first_air_date, is_special):
    logging.debug(u'Cache - Create Amazon TV Show Season [%s - season %s]', tmdb_id, season)

    try:

        if first_air_date is None:
            first_air_date = '2099-12-31'

        engine.execute("INSERT INTO tvshows_amazon_seasons (tmdb_id, country, lang, season_amazon_id, season, "
                       "overview, title, url, price_season_hd, price_season_sd, "
                       "rent_season_hd, rent_season_sd, preorder_season_hd, "
                       "preorder_season_sd, episode_count, currency, first_air_date, is_special) "
                       "VALUES "
                       "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'"
                       ", '%s', '%s', '%s', '%s')"
                       % (
                           tmdb_id, country.lower(), lang, Functions.format_sql(season_amazon_id), season,
                           Functions.format_sql(overview), Functions.format_sql(title), Functions.format_sql(url),
                           price_season_hd, price_season_sd, rent_season_hd, rent_season_sd,
                           preorder_season_hd, preorder_season_sd, episode_count, currency, first_air_date, is_special))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_amazon_season(tmdb_id, country, lang, season_amazon_id, season, overview, title, url,
                         price_season_hd, price_season_sd, rent_season_hd, rent_season_sd, preorder_season_hd,
                         preorder_season_sd, episode_count, currency, first_air_date, is_special):
    try:

        engine.execute(
            "UPDATE tvshows_amazon_seasons SET title='%s', overview='%s', url='%s', "
            "currency='%s', season_amazon_id='%s',"
            "price_season_hd='%s', price_season_sd='%s', rent_season_hd='%s', rent_season_sd='%s', "
            "preorder_season_hd='%s', preorder_season_sd='%s'"
            "WHERE tmdb_id='%s' AND season='%s' AND lang='%s' AND country='%s'"
            % (
                Functions.format_sql(title), Functions.format_sql(overview), Functions.format_sql(url), currency,
                season_amazon_id,
                price_season_hd, price_season_sd,
                rent_season_hd, rent_season_sd,
                preorder_season_hd, preorder_season_sd,
                tmdb_id, season, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


# ----- Search for Amazon Episode
def search_tvshow_episode_amazon(tmdb_id, country, lang, season, episode, title=None):
    try:
        result = engine.execute(
            "SELECT * FROM tvshows_amazon_episodes "
            "WHERE "
            "tmdb_id=(%s) AND country=(%s) AND lang=(%s) AND season=(%s) AND episode=(%s)",
            (tmdb_id, country.lower(), lang, season, episode))
        if result.rowcount > 0:
            return dict(result.fetchone())

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def add_amazon_episode(tmdb_id, country, lang, season_amazon_id, season, episode, overview, title, url, is_prime,
                       is_preorder, currency, price_episode_hd, price_episode_sd, rent_episode_hd, rent_episode_sd,
                       preorder_episode_hd, preorder_episode_sd, package):
    logging.debug(u'Cache - Create Amazon TV Show Episode- [%s - season %s ep.%s]', tmdb_id, season, episode)

    try:

        engine.execute(
            "INSERT INTO tvshows_amazon_episodes (tmdb_id, country, lang, season_amazon_id, season, episode, "
            "overview, title, url, is_prime, is_preorder, currency, price_episode_hd, price_episode_sd, "
            "rent_episode_hd, rent_episode_sd, preorder_episode_hd, "
            "preorder_episode_sd, package) "
            "VALUES "
            "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s'"
            ", '%s', '%s', '%s', '%s', '%s')"
            % (
                tmdb_id, country.lower(), lang, Functions.format_sql(season_amazon_id), season, episode,
                Functions.format_sql(overview), Functions.format_sql(title), Functions.format_sql(url), is_prime,
                is_preorder, currency, price_episode_hd, price_episode_sd, rent_episode_hd, rent_episode_sd,
                preorder_episode_hd, preorder_episode_sd, package))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_amazon_episode(tmdb_id, country, lang, season_amazon_id, season, episode, overview, title, url, is_prime,
                          is_preorder, currency, price_episode_hd, price_episode_sd, rent_episode_hd, rent_episode_sd,
                          preorder_episode_hd, preorder_episode_sd, package):
    try:

        engine.execute(
            "UPDATE tvshows_amazon_episodes SET title='%s', overview='%s', url='%s', "
            "currency='%s', season_amazon_id='%s', is_prime='%s', is_preorder='%s', "
            "price_episode_hd='%s', price_episode_sd='%s', rent_episode_hd='%s', rent_episode_sd='%s', "
            "preorder_episode_hd='%s', preorder_episode_sd='%s', package='%s'"
            "WHERE tmdb_id='%s' AND season='%s' AND episode='%s' AND lang='%s' AND country='%s'"
            % (
                Functions.format_sql(title), Functions.format_sql(overview), Functions.format_sql(url), currency,
                season_amazon_id, is_prime, is_preorder, price_episode_hd, price_episode_sd, rent_episode_hd,
                rent_episode_sd,
                preorder_episode_hd, preorder_episode_sd, package, tmdb_id, season, episode, lang, country.lower()))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Hulu Movies#######################################

def search_release_hulu(tmdb_id):

    try:
        result = engine.execute("SELECT * FROM release_hulu WHERE tmdb_id=(%s)",(tmdb_id))

        if result.rowcount > 0:
            return dict(result.fetchone())
        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def create_release_hulu(tmdb_id, guidebox_id, ios_link, ios_source, web_link, web_source):

    logging.debug(u'Cache - Create hulu release - [%s]' % tmdb_id)

    try:
        engine.execute("INSERT INTO release_hulu (tmdb_id, guidebox_id, ios_link, ios_source, web_link, web_source)\
                       VALUES ('%s', '%s', '%s', '%s', '%s', '%s')"
                       % (tmdb_id, guidebox_id, ios_link, ios_source, web_link, web_source))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def update_release_hulu(tmdb_id, guidebox_id, ios_link, ios_source, web_link, web_source):

    logging.debug(u'Cache - Update hulu release - [%s]' % tmdb_id)

    try:
        engine.execute("UPDATE release_hulu SET guidebox_id='%s', ios_link='%s', ios_source='%s', web_link='%s'\
                        , web_source='%s' WHERE tmdb_id='%s'"
                       % (guidebox_id, ios_link, ios_source, web_link, web_source, tmdb_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


####################################### Hulu Tv Shows#######################################

def search_tvshow_hulu(tmdb_id):

    try:
        result = engine.execute("SELECT * FROM tvshows_hulu WHERE tmdb_id=(%s)",(tmdb_id))

        if result.rowcount > 0:
            return dict(result.fetchone())
        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")

def add_update_hulu_tvshow(tmdb_id, guidebox_id, session, found_in_cache):

    try:
        if found_in_cache:
            session.execute("UPDATE tvshows_hulu SET guidebox_id='%s' WHERE tmdb_id='%s'"
                           % (guidebox_id, tmdb_id))
        else:
            session.execute("INSERT INTO tvshows_hulu (tmdb_id, guidebox_id)\
                VALUES ('%s', '%s')"
                     % (tmdb_id, guidebox_id))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_episode_hulu_by_tmdbid(tmdb_id, season_number, episode_number):

    try:
        result = engine.execute("SELECT * FROM tvshows_hulu_episodes WHERE tmdb_id=(%s) AND season_number=(%s) "
                                "AND episode_number=(%s)",(tmdb_id, season_number, episode_number))

        if result.rowcount > 0:
            return dict(result.fetchone())
        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")


def search_tvshow_episode_hulu(episode_gbox_id):

    try:
        result = engine.execute("SELECT * FROM tvshows_hulu_episodes WHERE episode_gbox_id=(%s)",(episode_gbox_id))

        if result.rowcount > 0:
            return dict(result.fetchone())
        else:
            return None

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")

def add_update_hulu_episode(tmdb_id, tvshow_gbox_id, episode_gbox_id, season_number, episode_number, content_type,
                            web_source, web_link, ios_source, ios_link, session, found_in_cache, lookup_done
                                           ):
    try:
        if found_in_cache:
            session.execute("UPDATE tvshows_hulu_episodes SET tmdb_id='%s', tvshow_gbox_id='%s', season_number='%s', "
                            "episode_number='%s', content_type='%s', web_source='%s', web_link='%s', ios_source='%s', "
                            "ios_link='%s', lookup_done='%s' WHERE episode_gbox_id='%s'"
                           % (tmdb_id, tvshow_gbox_id, season_number, episode_number, content_type,
                            web_source, web_link, ios_source, ios_link, lookup_done, episode_gbox_id))
        else:
            session.execute("INSERT INTO tvshows_hulu_episodes (tmdb_id, tvshow_gbox_id, episode_gbox_id, season_number, "
                            "episode_number, content_type, web_source, web_link, ios_source, ios_link, lookup_done )\
                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                     % (tmdb_id, tvshow_gbox_id, episode_gbox_id, season_number, episode_number, content_type, web_source,
                        web_link, ios_source, ios_link, lookup_done))

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot",
                           ":warning:")
