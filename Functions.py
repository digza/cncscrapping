# coding: utf-8

import difflib
import hashlib
import logging
import re
import unicodedata
import ftfy
import Slack
import inspect


# Return similarity of two titles
def compare_title(title1, title2):
    result = compare_string(clean_string(title1), clean_string(title2))
    logging.debug("Title similarity: %s" % result)
    return result


# Return similarity of two strings
def compare_string(str1, str2):
    result = difflib.SequenceMatcher(a=str1.lower(), b=str2.lower()).ratio()
    logging.debug("String similarity: %s" % result)
    return result


# Clean a string from special character
def clean_string(s, option=None):
    newtitle = ftfy.fix_text(s)
    newtitle = newtitle.lower()
    newtitle = remove_accents(newtitle)

    if option == 'plex':
        newtitle = re.sub(r'[?|$|!:”‘’,“”„&]', r' ', newtitle)

    else:
        newtitle = re.sub(r'[?|$|!:”‘’\',“”„&-]', r' ', newtitle)

    newtitle = re.sub("\s\s+", " ", newtitle)

    return newtitle


# Replace special character by ascii character from a string
def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


# Clean a string for itunes search
def clean_itunes(string, lang):
    # Clean string
    newtitle = clean_string(string)

    # Clean exception for french
    if lang == 'fr':
        newtitle = newtitle.replace("1ere partie", "")
        newtitle = newtitle.replace("2eme partie", "")
        newtitle = newtitle.replace("1re partie", "")
        newtitle = newtitle.replace("2e partie", "")
        newtitle = newtitle.replace("3eme partie", "")

        newtitle = newtitle.replace("vol.", "")
        newtitle = newtitle.replace("volume", "")
        newtitle = newtitle.replace("chapitre", "")

        newtitle = re.sub(r'\si($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\sii($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\siii($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\siv($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\sv($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\s1($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\s2($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\s3($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\s4($|\s)', r' ', newtitle)
        newtitle = re.sub(r'\s5($|\s)', r' ', newtitle)

        newtitle = newtitle.replace("die hard", "")

    newtitle = re.sub("\s\s+", " ", newtitle)
    return newtitle


# ----- Return float from a unicode string
def get_decimal(text):
    converted = re.search(r"(\d*\,\d*)|(\d*\.\d*)|\d+", text)
    converted = (converted.group(0))
    converted = re.sub(r"\,", ".", converted)
    return float(converted)


# ----- Hash function
def hash_fct(w):
    if w:
        w = remove_accents(w)
        return hashlib.md5(w.lower()).hexdigest()


# ----- Transform single quote to double quote for sql insert
def format_sql(w):
    if w:
        w = w.replace("%", "%%")
        w = w.replace("'", "''")
        return w


# ----- Get source language and id from plex guid
def parse_plex_guid(guid):
    try:
        if guid:
            if re.match(u'com.plexapp.agents.', guid):

                # Identify source
                source = re.sub(u'com.plexapp.agents.','', guid)
                source = re.sub(u'://.*','', source)

                # Identify language
                lang = re.sub(u'.*[?]lang=','', guid)

                #Identify ID
                id = re.sub(u'[?]lang=.*','', guid)
                id = re.sub(u'.*://','', id)

                #If it's an episode, parse into id, season, episode
                if re.match(u'\d+[/]\d+[/]\d+', id):
                    season = re.sub(u'^\d+[/]', '', id)
                    season = int(re.sub(u'[/]\d+$', '', season))
                    episode = int(re.sub(u'^\d+[/]\d+[/]', '', id))
                    id = re.sub(u'[/]\d+[/]\d+$', '', id)

                    return {u'source': source, u'id': id, u'lang': lang, u'season': season, u'episode': episode}

                return {u'source': source, u'id': id, u'lang': lang}

    except Exception as e:
        logging.error("[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message)
        #Slack.send_message("#errorpython", "[" + __name__ + "]  [" + inspect.stack()[0][3] + "]: " + e.message, "PyBot", ":warning:")
