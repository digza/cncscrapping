# -*- coding: utf-8 -*-

from sqlalchemy.engine.url import URL
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

import Cnc_class
from Cnc_class import CNC_movies


############################ DB Connection ########################


# ----- Creates SQLAlchemy session
def create_session():
    engine = create_engine(URL(**Cnc_class.DATABASE),
                           pool_size=0,
                           max_overflow=-1)
    session = scoped_session(sessionmaker())
    session.remove()
    session.configure(bind=engine, autoflush=False, expire_on_commit=False)

    return session


# ----- Commits changes on DB
def commit(session):
    session.commit()


def close(session):
    session.close()

#############################CNC - Movies #########################


# ----- Create or update a release in DB, if need be
def create_cnc_release(movie_data, providers):
    session = create_session()

    for key in providers:

        # --- No offers and primary key not in DB, move on to next provider
        if providers[key]['lowest_rent_price'] is None \
                and providers[key]['lowest_buy_price'] is None \
                and not session.query(CNC_movies).filter(
                    CNC_movies.pkey ==
                    str(movie_data['tmdb_id'])
                    + providers[key]['provider'] + '-'
                    + movie_data['country'] + '-'
                    + movie_data['lang']).count():
            continue
        # --- Create/update release in DB
        else:
            movie = CNC_movies(movie_data, providers[key])
            session.merge(movie)

    commit(session)
    close(session)

