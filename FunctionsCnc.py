from lxml import html
import requests
import requests.packages.urllib3
from datetime import datetime, timedelta

from FunctionsCncCache import create_session, create_cnc_release, close
from Movie import Movie, compare
from CncTools import proper_len_format, remove_accents
from CncTools import rm_url_dup, cleanse_movie_data, update_dictionary
from Cnc_class import CNC_movies


# ----- Gets the four movies url from the search page
def get_movies_url(pages):
    url_list = []
    for url in pages:
        if url[:6] == 'title/':
            url_list.append(url)
    return url_list


# ----- Make title fit the research requirements
def get_request_title(title):

    try :
        title_split = title.split()
        request_title = ''

        for index in range(0, len(title_split) - 1):
            request_title += title_split[index] + '%20'

        request_title += title_split[len(title_split) - 1]
        request_title = 'http://vad.cnc.fr/titles?search=' + \
                        request_title + '&sort=pertinence&rsz=4&start='

    except AttributeError:
        return False
    return request_title


# ----- Get movie basic info
def get_movies_info(title, year_len, origin):
    data = {}

    data['title'] = title
    data['origin'] = origin
    data['year'] = year_len[0]
    data['length'] = [year_len[1], year_len[2]]

    data = proper_len_format(data)
    return data


# ----- Explicit
def set_providers_name(providers):
    for key in providers:
        providers[key]['provider'] = key
    return providers


# ----- Get providers and the offers they feature
def get_cnc_offers(movie_url):
    url = requests.get('http://vad.cnc.fr/' + movie_url, verify=False)
    tree = html.fromstring(url.content)

    new_offer = []
    new_list = []
    providers = {
        'LaCinetek': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'CanalPlay': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'FilmoTV': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'FranceTV': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Imineo': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Fnac': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'MyTf1VOD': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Orange': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'SFR': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Nolim Films': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'UniversCine': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Videofutur': {'lowest_rent_price': None, 'lowest_buy_price': None},
        'Wuaki.tv': {'lowest_rent_price': None, 'lowest_buy_price': None}
    }

    providers = set_providers_name(providers)
    offer = tree.xpath('//a/@onclick')

    for elem in offer:
        elem = elem.split(',')
        elem = elem[2:4]

        if type(elem[0]) == unicode:
            elem = remove_accents(elem[0] + remove_accents(elem[1]))

        else:
            elem = elem[0] + elem[1]

        elem = elem.replace('\'', '')
        new_offer.append(elem)

    # --- Get price and remove euro symbol
    price = tree.xpath(
        '//span[@class="btn-payment-choice-text-no-bold"]/text()')

    for i in range(0, len(price)):
        price[i] = price[i].encode('ascii', 'ignore')
        new_list.append([new_offer[i], price[i][:len(price[i]) - 1]])

    # --- Save providers and prices in providers
    for i in range(0, len(new_list)):

        if "FTV" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['FranceTV']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['FranceTV']['lowest_buy_price'] = new_list[i][1]

        elif "Videofutur" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Videofutur']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Videofutur']['lowest_buy_price'] = new_list[i][1]

        elif "MYTF1" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['MyTf1VOD']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['MyTf1VOD']['lowest_buy_price'] = new_list[i][1]

        elif "FNAC" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Fnac']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Fnac']['lowest_buy_price'] = new_list[i][1]

        elif "FilmoTV" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['FilmoTV']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['FilmoTV']['lowest_buy_price'] = new_list[i][1]

        elif "SFR" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['SFR']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['SFR']['lowest_buy_price'] = new_list[i][1]

        elif "Wuaki" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Wuaki.tv']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Wuaki.tv']['lowest_buy_price'] = new_list[i][1]

        elif "Nolim" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Nolim Films']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Nolim Films']['lowest_buy_price'] = new_list[i][1]

        elif "CANALPLAY" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['CanalPlay']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['CanalPlay']['lowest_buy_price'] = new_list[i][1]

        elif "Orange" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Orange']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Orange']['lowest_buy_price'] = new_list[i][1]

        elif u'UniversCin' in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['UniversCine']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['UniversCine']['lowest_buy_price'] = new_list[i][1]

        elif "Imineo" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['Imineo']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['Imineo']['lowest_buy_price'] = new_list[i][1]

        elif "LaCinetek" in new_list[i][0]:

            if 'Louer' in new_list[i][0]:
                providers['LaCinetek']['lowest_rent_price'] = new_list[i][1]
            elif 'Acheter' in new_list[i][0]:
                providers['LaCinetek']['lowest_buy_price'] = new_list[i][1]

    # --- Get links
    links = tree.xpath('//a[@class="col-xs-12 col-sm-12 col-md-12 col-lg-12 '
                       'btn-payment-choice text-center"]/@href')
    links = rm_url_dup(links)

    # --- Save links in providers
    for elem in links:

        if "imineo" in elem:
            providers["Imineo"]['url'] = elem

        elif "orange" in elem:
            providers["Orange"]['url'] = elem

        elif "francetv" in elem:
            providers["FranceTV"]['url'] = elem

        elif "sfr" in elem:
            providers["SFR"]['url'] = elem

        elif "mytf1" in elem:
            providers["MyTf1VOD"]['url'] = elem

        elif "universcine" in elem:
            providers["UniversCine"]['url'] = elem

        elif "filmotv" in elem:
            providers["FilmoTV"]['url'] = elem

        elif "wuaki" in elem:
            providers["Wuaki.tv"]['url'] = elem

        elif "videofutur" in elem:
            providers["Videofutur"]['url'] = elem

        elif "nolim" in elem:
            providers["Nolim Films"]['url'] = elem

        elif "canalplay" in elem:
            providers["CanalPlay"]['url'] = elem

        elif "fnac" in elem:
            providers["Fnac"]['url'] = elem

        # LACINETEK DOESNT WORK ATM
        elif "tous-les-films" in elem:
            providers["LaCinetek"]['url'] = elem
    return providers


# ----- Checks if movie confidence >= 0.9
def check_movie_confidence(ref_movie, movie):
    new_movie = Movie(title=movie['title'], year=str(movie['year']),
                      original_title=movie['origin'], lang=u'fr')

    if compare(ref_movie, new_movie) >= 0.9:
        return movie
    return {}


# ----- Browse CNC pages and search for the appropriate movie
def fetch_cnc_movie(ref_movie):
    iteration = 0

    if get_request_title(ref_movie.title) == False:
        return {}, {}

    result = {}

    # ----- TO IMPROVE
    while iteration <= 16:
        url = requests.get(
            get_request_title(ref_movie.title) + str(iteration))
        tree = html.fromstring(url.content)
        movie_titles = tree.xpath('//a[@class]/span/text()')
        movie_year_len = cleanse_movie_data(
            tree.xpath('//div[@class="bold"]/span/text()'))
        movie_origin = tree.xpath('//div/span[@class="bold"]/text()')
        pages = rm_url_dup(get_movies_url(tree.xpath('//a/@href')))

        for index in range(0,len(pages)):

            movie = get_movies_info(movie_titles[index + 2],
                                    movie_year_len[index],
                                    movie_origin[index])

            if len(movie):
                result = check_movie_confidence(ref_movie, movie)

            if len(result):
                providers = get_cnc_offers(pages[index])
                result['tmdb_id'] = ref_movie.tmdb_id
                result = update_dictionary(result, 'movie')
                return result, providers

        iteration += 4
    return result, {}

# ----- Checks if a movie is in db / if data are outdated
def is_in_db(tmdb_id):
    session = create_session()
    movie_list = session.query(CNC_movies).filter(
        CNC_movies.tmdb_id == tmdb_id)

    if not movie_list.count():
        close(session)
        return False

    for movie in movie_list:

        # --- If movie not available or info is outdated (24h+)
        if movie.lowest_rent_price is None and movie.lowest_buy_price is None:
            close(session)
            return False

        elif movie.date_update + timedelta(days=1) < datetime.now():
            close(session)
            return False

    close(session)
    return True


def get_cnc_providers(tmdb_id):

    if not is_in_db(tmdb_id):
        ref_movie = Movie(tmdb_id=tmdb_id, lang=u'fr')
        result = tuple(fetch_cnc_movie(ref_movie))
        create_cnc_release(result[0], result[1])


def get_json(tmdb_id, ref_provider):

    session = create_session()

    query = session.query(CNC_movies).filter(
        CNC_movies.pkey == str(tmdb_id)
        + ref_provider + '-'
        + 'FR-'
        + u'fr')

    if query.count() == 1:
        for movie in query:
            result = {
                'date_creation': movie.date_creation,
                'date_update': movie.date_update,
                'currency': movie.currency,
                'country': movie.country,
                'lang': movie.lang,
                'lowest_rent_price': movie.lowest_rent_price,
                'lowest_buy_price': movie.lowest_buy_price,
                'tmdb_id': movie.tmdb_id,
                'title': movie.title,
                'url': movie.url
                }
    else:
        result = {}

    session.close()
    return result
