# -*- coding: latin-1 -*-

import unittest
from CncTools import _remove_accents_, _proper_len_format_, _rm_url_dup_, _get_max_index_
from FunctionsCnc import _get_movies_url_, _get_request_title_, _get_cnc_providers_, _fetch_cnc_movie_
import requests
from lxml import html
import requests.packages.urllib3

requests.packages.urllib3.disable_warnings()

class CncTest(unittest.TestCase):
    
    # ----- Testing accent removal - simple
    def test01_remove_accents_simple(self):
        str = u'bonjour j\'aime le pat�'
        str2 = _remove_accents_(str)

        self.assertEqual('bonjour j\'aime le pate', str2)

    # ----- Testing accent removal - harder
    def test02_remove_accents_harder(self):
        str = u'��������'
        str2 = _remove_accents_(str)

        self.assertEqual('eeeeaaau', str2)

    # ----- Testing accent removal - evil
    def test03_remove_accents_evil(self):
        str = u'�������������'
        str2 = _remove_accents_(str)

        self.assertEqual('eeeeaaauiiuoc', str2)

    # ----- Testing movie length format - simple
    def test04_len_format_simple(self):
        dic = {
        'length' : [1, 12]
        }
        _proper_len_format_(dic)

        self.assertEqual(dic['length'], '1h12min')

    # ----- Testing if minutes = 02 instead of 2
    def test05_len_format_minutes(self):
        dic = {
        'length' : [0, 2]
        }
        _proper_len_format_(dic)

        self.assertEqual(dic['length'], '0h02min')

    # ----- Testing if duplicates are removed - only dup
    def test06_urldup_onlydup(self):
        list = ["http://42.com", "http://42.com", "http://42.com"]
        list = _rm_url_dup_(list)
        list2 = ["http://42.com"]

        self.assertEqual(list, list2)

    # ----- Testing if duplicates are removed - one dup + others
    def test07_urldup_onedup(self):
        list = ["http://42.com", "http://42.com", "http://43.com",
                "http://44.com"]
        list = _rm_url_dup_(list)
        list2 = ["http://42.com", "http://43.com", "http://44.com"]

        self.assertEqual(list, list2)

    # ----- Testing if duplicates are removed - loads of dups
    def test08_urldup_multi(self):
        list = ["http://42.com", "http://42.com", "http://43.com",
                "http://44.com", "http://42.com", "http://42.com",
                "http://43.com", "http://43.com", "http://42.com",
                "http://42.com", "http://44.com", "http://44.com"]
        list = _rm_url_dup_(list)
        list2 = ["http://42.com", "http://43.com", "http://44.com"]

        self.assertEqual(list, list2)

    # ----- Testing get max index - start
    def test09_maxindex_start(self):
        list = ['Ma Recherche', '42', 'riri', 'fifi', 'loulou']
        nb = int(_get_max_index_(list))

        self.assertEqual(42, nb)

    # ----- Testing get max index - end
    def test10_maxindex_end(self):
        list = ['riri', 'fifi', 'loulou', 'Ma Recherche', '42']
        nb = int(_get_max_index_(list))

        self.assertEqual(42, nb)

    # ----- Testing get max index - middle
    def test11_maxindex_mid(self):
        list = ['riri', 'Ma Recherche', '42', 'fifi', 'loulou']
        nb = int(_get_max_index_(list))

        self.assertEqual(42, nb)

    # ----- Testing if we get only the movies url - one valid
    def test12_movie_url(self):
        list = ['hello', 'world', 'title/elkjnklejrlcekjwrlkejnrc']
        list = _get_movies_url_(list)

        self.assertEqual(['title/elkjnklejrlcekjwrlkejnrc'], list)

    # ----- Testing if we get only the movies url - invalid + one valid
    def test13_movie_url_2(self):
        list = ['hello', 'world', 'title/elkjnklejrlcekjwrlkejnrc',
                'ttle/jwdlkqjwdlkqhwflkqhef', '?title/akjfqeljfqe']
        list = _get_movies_url_(list)

        self.assertEqual(['title/elkjnklejrlcekjwrlkejnrc'], list)

    # ----- Testing if we get only the movies url - invalid + one letter + empty
    def test14_movie_url_3(self):
        list = ['hello', 'world', 'title/elkjnklejrlcekjwrlkejnrc',
                'ttle/jwdlkqjwdlkqhwflkqhef', '?title/akjfqeljfqe',
                'title/s', 'tritle/efkjqeawdaef', 'title/']
        list = _get_movies_url_(list)

        self.assertEqual(['title/elkjnklejrlcekjwrlkejnrc', 'title/s', 'title/'], list)
        
    
    # ----- Testing if title is formated correctly
    def test15_request_title(self):
        title = 'La Grande Vadrouille'
        title = _get_request_title_(title)
        
        self.assertEqual('http://vad.cnc.fr/titles?search=' + "La%20Grande%20Vadrouille" +
                         "&mode=2&mode=7&mode=3&mode=4&sort=pertinence&rsz=4&start=", title)

    # ----- Testing the whole process - #1    
    def test16_whole_process_1(self):
        cnc_movie = _get_cnc_providers_(213, {})

        # ----- Testing the whole process - #1 - original title
        ref = 'North by Northwest'        
        self.assertEqual(ref, cnc_movie['origin'])

        # ----- Testing the whole process - #1 - title
        ref = 'La Mort aux trousses'
        self.assertEqual(ref, cnc_movie['title'])

        # ----- Testing the whole process - #1 - length
        ref = '2h16min'
        self.assertEqual(ref, cnc_movie['length'])

        # ----- Testing the whole process - #1 - year
        ref = '1959'
        self.assertEqual(ref, cnc_movie['year'])

        # ----- Testing the whole process - #1 - providers
        ref = ['MyTf1VOD', 'FilmoTV', 'Fnac', 'Nolim Films', 'Wuaki.tv', 'SFR', 'FranceTV', 'Videofutur']
        ordered = []

        for key in cnc_movie['Providers']:
            if len(cnc_movie['Providers'][key]):
                ordered.append(key)

        # ----- Checking if ref and prov list have the same length
        self.assertEqual(len(ordered), len(ref))
        
        # ----- Checking each elements
        for i in range(0, len(ref)):
            self.assertEqual(ordered[i], ref[i])
        
    # ----- Testing the whole process - #2
    def test17_whole_process_2(self):
        cnc_movie = _get_cnc_providers_(2786, {})

        # ----- Testing the whole process - #2 - original title
        ref = 'Pierrot le Fou'
        self.assertEqual(ref, cnc_movie['origin'])

        # ----- Testing the whole process - #2 - title
        ref = 'Pierrot le Fou'        
        self.assertEqual(ref, cnc_movie['title'])

        # ----- Testing the whole process - #2 - length
        ref = '1h55min'
        self.assertEqual(ref, cnc_movie['length'])

        # ----- Testing the whole process - #2 - year
        ref = '1965'
        self.assertEqual(ref, cnc_movie['year'])

        # ----- Testing the whole process - #2 - providers
        ref = ['UniversCine', 'LaCinetek', 'MyTf1VOD', 'Orange', 'FilmoTV', 'SFR', 'Imineo', 'FranceTV', 'Videofutur']
        ordered = []

        for key in cnc_movie['Providers']:
            if len(cnc_movie['Providers'][key]):
                ordered.append(key)

        self.assertEqual(len(ordered), len(ref))

        for i in range(0, len(ref)):
            self.assertEqual(ordered[i], ref[i])
            
if __name__ == '__main__':
    unittest.main(verbosity=3)
