# coding: utf-8

import datetime
import logging
import math
import time
import Functions
import FunctionsCache
import FunctionsTmdb
import Settings
import omdb
import tmdbsimple as tmdb

tmdb.API_KEY = Settings.TMDB_API_KEY


class TvShow:
    def __init__(self, title=None, year=None, original_title=None, lang=u'en', tmdb_id=0, imdb_id=None,
                 guess_id=True, fetch_data=True, season=None):


        # Save raw data
        self.forced_title = title
        self.forced_original_title = original_title
        self.lang = lang[:2].lower()
        self.forced_year = year
        self.season = season

        # Convert year to integer
        if year is not None:
            self.forced_year = int(year)

        # TMDB
        self.tmdb_id = tmdb_id
        self.tmdb_title = None
        self.tmdb_original_title = None
        self.tmdb_year = None
        self.tmdb_overview = None
        self.tmdb_poster_path = None
        self.tmdb_backdrop_path = None
        self.tmdb_number_of_seasons = 0
        self.tmdb_first_air_date = "12-31-2099"
        self.seasons = None
        self.seasons_json= []

        # IMDB
        self.imdb_id = imdb_id

        # Scores
        self.score_rt_meter = None
        self.score_metacritic = None
        self.score_imdb = 0
        self.score_tmdb = 0

        # Status boolean
        self.fetched_from_cache = False
        self.fetched_from_sources = False
        self.seasons_fetched_from_cache = False
        self.cached = {}
        self.result_season = None

        self.init_json()

        # Guess tmdb_id from name
        if not tmdb_id and guess_id:
            self.find_tmdb_id()

        # Fetch data of the movie (best source available)
        if self.tmdb_id and fetch_data:
            t1 = time.time()
            self.fetch_data()
            logging.info("[Profiling][TvShow]fetch_data took: %s", time.time() - t1)

    # ---- Look for tmdb ID from title and language (and save clean title, original, title and year)
    def find_tmdb_id(self):
        logging.debug('TVSHOW - find_tmdb_id')
        t1 = time.time()
        if not self.tmdb_id and self.imdb_id:
            self.tmdb_id = FunctionsCache.get_tvshow_tmdb_from_imdb(imdb_id=self.imdb_id)

        # Look in the cache
        if not self.tmdb_id and (self.title or self.original_title):
            self.tmdb_id = FunctionsCache.search_tvshow_id(title=self.title, original_title=self.original_title,
                                                           lang=self.lang)

        # Look in tmdb and add in cache if found for faster future lookup
        if not self.tmdb_id and (self.title or self.original_title):
            self.tmdb_id = FunctionsTmdb.search_tvshow_id(title=self.title, original_title=self.original_title,
                                                          lang=self.lang)

            FunctionsCache.add_tvshow_search_in_cache(tmdb_id=self.tmdb_id, lang=self.lang,
                                                      original_title=self.original_title, title=self.title)
        logging.info("[Profiling]find_tmdb_id took: %s", time.time() - t1)


    # ---- Fetch data from cache then from tmdb
    # if season is given it will go throw seasons as well till the searched is found
    # if episode is given, same
    def fetch_data(self):

        if self.tmdb_id and not self.fetched_from_cache:

            self.fetch_from_cache()

            # Check if an update is needed (cache older than a week)
            if self.fetched_from_cache:
                logging.debug("[TvShow][tmdb_%s]Already in cache...", self.tmdb_id)
                diff = datetime.datetime.now() - self.cached['date_update']
                if diff.days > 1:
                    logging.debug("[TvShow]fetch_data, cache needs update")
                    self.fetch_tvshow_from_sources()
                    self.save_in_cache()
                else:
                    #cache a jour, faut construire le res
                    self.seasons = FunctionsCache.fetch_tvshow_seasons_data(tmdb_id=self.tmdb_id, lang=self.lang)
                    non_special = FunctionsCache.fetch_tvshow_seasons_number(tmdb_id=self.tmdb_id, lang=self.lang,
                                                                              is_special=False)
                    if non_special['count'] != self.tmdb_number_of_seasons:
                        self.fetch_tvshow_from_sources()
                        self.save_in_cache()
                    else:
                        self.seasons_fetched_from_cache = True
            else:
                logging.debug("[TvShow]fetch_data, not found in cache...")
                self.fetch_tvshow_from_sources()
                self.save_in_cache()

            found_match = False
            session = FunctionsCache.createSession()
            if self.seasons:
                for season in self.seasons:
                    #{u'season_number': 0, u'poster_path': None, u'id': 28541, u'episode_count': 13, u'air_date': u'2009-03-16'}
                    if not season.has_key('date_update'):
                        season['date_update'] = datetime.datetime.now()
                    if not season.has_key('date_creation'):
                        season['date_creation'] = datetime.datetime.now()
                    if not season.has_key('air_date'):
                        season['air_date'] = "12-31-2099"

                    # if we are looking for a specific season, ignore non matching ones
                    if self.season is not None and self.season != season['season_number'] and self.fetched_from_cache:
                        logging.debug("[TvShow]Season %s not matching skip...", season['season_number'])
                        continue

                    # continue updating seasons and episodes data
                    s_ret = TMDBSeason(tmdb_id=self.tmdb_id, lang=self.lang, season_obj=season,
                                       searched_season = self.season, session=session,
                                       fetched_from_cache=self.seasons_fetched_from_cache)
                    if self.season is not None and self.season == season['season_number']:
                        self.seasons_json.append(s_ret.build_json())
                        self.result_season = s_ret
                        found_match = True
                        #break


            if not found_match and self.season:
                # nothing found, create dummy one
                null_season = {}
                null_season['season_number']=self.season;
                self.result_season = TMDBSeason(tmdb_id=self.tmdb_id,  season_obj=null_season, lang=self.lang)
            FunctionsCache.commit(session)
    # ---- Fetch TV Show data from cache
    def fetch_from_cache(self):

        self.cached = FunctionsCache.fetch_tvshow_data(self.tmdb_id, self.lang)

        if self.cached:
            self.build_me()
            self.fetched_from_cache = True


    def build_me(self):
        self.tmdb_title = self.cached['title']
        self.tmdb_first_air_date = self.cached['first_air_date']
        self.tmdb_original_title = self.cached['original_title']
        self.tmdb_poster_path = self.cached['poster_path']
        self.tmdb_number_of_seasons = self.cached['number_of_seasons']
        self.tmdb_overview = self.cached['overview']
        self.tmdb_backdrop_path = self.cached['backdrop_path']
        self.imdb_id = self.cached['imdb_id']
        self.score_rt_meter = self.cached['score_rt_meter']
        self.score_metacritic = self.cached['score_metacritic']
        self.score_imdb = self.cached['score_imdb']
        self.score_tmdb = self.cached['vote']
        self.last_update = self.cached['date_update']


    # ---- Fetch movie data from sources
    def fetch_tvshow_from_sources(self):

        t1 = time.time()
        if self.tmdb_id and self.lang:

            # Fetch from tmdb
            result_tmdb = FunctionsTmdb.fetch_tvshow_data(self.tmdb_id, self.lang)

            if result_tmdb:
                self.tmdb_title = result_tmdb['name']
                self.tmdb_original_title = result_tmdb['original_name']
                self.tmdb_first_air_date = result_tmdb['first_air_date']
                self.tmdb_overview = result_tmdb['overview']
                self.score_tmdb = result_tmdb['vote_average']
                self.tmdb_poster_path = result_tmdb['poster_path']
                self.tmdb_backdrop_path = result_tmdb['backdrop_path']
                self.seasons = result_tmdb['seasons']
                self.imdb_id = result_tmdb['external_ids']['imdb_id']
                self.tmdb_number_of_seasons = result_tmdb['number_of_seasons']

                # If an imdb id is found fetch data from omdb
                if self.imdb_id:
                    result_omdb = omdb.imdbid(self.imdb_id, tomatoes=True)

                    if result_omdb:
                        self.score_rt_meter = result_omdb['tomato_meter']
                        self.score_metacritic = result_omdb['metascore']
                        self.score_imdb = result_omdb['imdb_rating']

            self.fetched_from_sources = True
        logging.info("[Profiling]fetch_tvshow_from_sources took: %s", time.time() - t1)

    # ----- Save TV Show in cache
    def save_in_cache(self):
        t1 = time.time()
        # If already in cache, update data
        if self.fetched_from_cache:
            FunctionsCache.update_tvshow(tmdb_id=self.tmdb_id, lang=self.lang, title=self.tmdb_title,
                                         original_title=self.tmdb_original_title, vote=self.score_tmdb,
                                         overview=self.tmdb_overview, poster_path=self.tmdb_poster_path,
                                         backdrop_path=self.tmdb_backdrop_path, first_air_date=self.tmdb_first_air_date,
                                         imdb_id=self.imdb_id, score_rt_meter=self.score_rt_meter,
                                         score_metacritic=self.score_metacritic, score_imdb=self.score_imdb,
                                         number_of_seasons=self.tmdb_number_of_seasons)

        # Otherwise create it
        elif self.fetched_from_sources:
            FunctionsCache.create_tvshow(tmdb_id=self.tmdb_id, lang=self.lang, title=self.tmdb_title,
                                         original_title=self.tmdb_original_title, vote=self.score_tmdb,
                                         overview=self.tmdb_overview, poster_path=self.tmdb_poster_path,
                                         backdrop_path=self.tmdb_backdrop_path, first_air_date=self.tmdb_first_air_date,
                                         imdb_id=self.imdb_id, score_rt_meter=self.score_rt_meter,
                                         score_metacritic=self.score_metacritic, score_imdb=self.score_imdb,
                                         number_of_seasons=self.tmdb_number_of_seasons)
        logging.info("[Profiling][TvShow]Cache add/update took: %s", time.time() - t1)

    # ----- Force cache update
    def force_cache_update(self):

        if self.tmdb_id:
            self.fetch_tvshow_from_sources()
            self.save_in_cache()


    # ----- Get TV Show info in a json formatted string
    # ---- Return result formatted in JSON
    def get_json(self):
        if self.season is not None:
            return self.result_season.build_json()
        else:
            return self.build_json()

    def build_json(self):

        if not self.cached:
            self.fetch_from_cache()
        if self.cached:
            if self.cached['title'] != u'None':
                self.cached["exist"] = True

            else:
                self.cached["exist"] = False

            self.cached['date_update'] = (self.cached['date_update']).strftime('%Y-%m-%d %H:%M:%S')
            self.cached['date_creation'] = (self.cached['date_creation']).strftime('%Y-%m-%d %H:%M:%S')
            self.cached['first_air_date'] = (self.cached['first_air_date']).strftime('%Y-%m-%d %H:%M:%S')
            self.cached["seasons"] = self.seasons
        else:
            self.cached["exist"] = False
        return self.cached


    def init_json(self):
        self.cached["seasons"] = self.seasons


    # ---- Return the title
    @property
    def title(self):

        if self.tmdb_title:
            return self.tmdb_title

        elif self.forced_title:
            return self.forced_title

        else:
            return None

    # ---- Return the year
    @property
    def year(self):

        if self.tmdb_year:
            return int(self.tmdb_year)

        elif self.forced_year:
            return int(self.forced_year)

        else:
            return None

    # ---- Return the original title
    @property
    def original_title(self):

        if self.tmdb_original_title:
            return self.tmdb_original_title

        elif self.forced_original_title:
            return self.forced_original_title

        else:
            return None

    # ---- Return the clean original title
    @property
    def clean_original_title(self):

        if self.tmdb_original_title:
            return Functions.clean_string(self.tmdb_original_title)

        elif self.forced_original_title:
            return Functions.clean_string(self.forced_original_title)

        else:
            return None

    # ---- Return the clean original title
    @property
    def clean_original_title_plex(self):

        if self.tmdb_original_title:
            return Functions.clean_string(self.tmdb_original_title, 'plex')

        elif self.forced_original_title:
            return Functions.clean_string(self.forced_original_title, 'plex')

        else:
            return None

    # ---- Return the clean title
    @property
    def clean_title(self):

        if self.tmdb_title:
            return Functions.clean_string(self.tmdb_title)

        elif self.forced_title:
            return Functions.clean_string(self.forced_title)

        else:
            return None

    # ---- Return the clean title
    @property
    def clean_title_plex(self):

        if self.tmdb_title:
            return Functions.clean_string(self.tmdb_title, 'plex')

        elif self.forced_title:
            return Functions.clean_string(self.forced_title, 'plex')

        else:
            return None

    # ---- Return the tmdb ID
    @property
    def tmdb_id(self):

        if self.tmdb_id:
            return self.tmdb_id

        else:
            return None

    # ---- Return the imdb ID
    @property
    def imdb_id(self):

        if self.imdb_id:
            return self.imdb_id

        else:
            return None

    # ---- Return the overview
    @property
    def overview(self):

        if self.tmdb_overview:
            return self.tmdb_overview

        else:
            return None

    # ---- Print movie with clean formatting
    def __str__(self):

        if self.tmdb_id and self.title and self.original_title and self.year:
            return ('%s (%s) [%s] [%s]' % (self.title, self.year, self.tmdb_id, self.original_title)).encode('utf8')

        elif self.title and self.original_title and self.year:
            return ('%s (%s) [%s]' % (self.title, self.year, self.original_title)).encode('utf8')

        elif self.title and self.year:
            return ('%s (%s)' % (self.title, self.year)).encode('utf8')

        elif self.original_title and self.year:
            return ('%s (%s)' % (self.original_title, self.year)).encode('utf8')

        elif self.title:
            return ('%s' % self.title).encode('utf8')

        elif self.original_title:
            return ('%s' % self.original_title).encode('utf8')

        else:
            return ''


# Return the match confidence between two movies
def compare(tvshow1, tvshow2):
    match_confidence = 0

    # Try to fetch movies ID
    tvshow1.find_tmdb_id()
    tvshow2.find_tmdb_id()

    # Use tmdb IDs if they are defined for both movies
    if tvshow1.tmdb_id and tvshow2.tmdb_id:
        if str(tvshow1.tmdb_id) == str(tvshow2.tmdb_id):
            match_confidence = 1

    # Use imdb IDs if they are defined for both movies
    elif tvshow1.imdb_id and tvshow2.imdb_id:
        if str(tvshow1.imdb_id) == str(tvshow2.imdb_id):
            match_confidence = 1

    # Otherwise try to identify with titles
    else:

        # Fetch_data for the tv shows
        tvshow1.fetch_data()
        tvshow2.fetch_data()

        # Calculate score for every combination
        if tvshow1.title and tvshow2.title:
            match_confidence = Functions.compare_title(unicode(tvshow1.title), unicode(tvshow2.title))

        if tvshow1.original_title and tvshow2.original_title:
            match_confidence = max(match_confidence,
                                   Functions.compare_title(unicode(tvshow1.original_title), unicode(tvshow2.original_title)))

        if tvshow1.title and tvshow2.original_title:
            match_confidence = max(match_confidence,
                                   Functions.compare_title(unicode(tvshow1.title), unicode(tvshow2.original_title)) * 0.9)

        if tvshow2.title and tvshow1.original_title:
            match_confidence = max(match_confidence,
                                   Functions.compare_title(unicode(tvshow2.title), unicode(tvshow1.original_title)) * 0.9)

        # Apply year coeff
        match_confidence *= get_year_coeff(tvshow1.year, tvshow2.year)

        # Apply seasons coeff
        match_confidence *= get_seasons_coeff(tvshow1.tmdb_number_of_seasons, tvshow2.tmdb_number_of_seasons)

    logging.info('Match confidence: {0:.0%}'.format(match_confidence))
    return match_confidence


# Return the coefficient for match confidence for years
def get_year_coeff(year1, year2):
    if year1 and year2:

        if year1 == year2:
            return 1

        if math.fabs(year1 - year2) == 1:
            return 0.95

        if math.fabs(year1 - year2) == 2:
            return 0.8

        else:
            return 0.3

    else:
        return 0.5


# Return the coefficient for match confidence for years
def get_seasons_coeff(nb_season1, nb_season2):
    if nb_season1 and nb_season2:

        if nb_season1 == nb_season2:
            return 1

        if math.fabs(nb_season2 - nb_season1) == 1:
            return 0.9

        else:
            return 0.2

    else:
        return 1


class TMDBSeason:
    def __init__(self, tmdb_id, lang=u'en', searched_season=None, fetched_from_cache=False, season_obj=None,
                 session=None):

        self.tmdb_id = tmdb_id
        self.lang = lang
        self.in_cache = False
        self.fetched_from_cache = fetched_from_cache
        self.fetched_from_sources = not fetched_from_cache
        self.episodes_fetched_from_cache = False
        self.season_obj = season_obj
        self.session = session

        self.fetch_from_cache()
        if not season_obj.has_key('id'):
            logging.debug("[TMDBSeason]Season null...")
            self.fetch_from_cache()
            self.save_in_cache()
            return

        self.searched_season = searched_season
        self.episodes_json = []
        self.episodes = None
        self.cached = None

        # Check if an update is needed (cache older than a week)
        if not self.fetched_from_cache:
            #season fetched from tv show sources
            logging.debug("[TMDBSeason]New season, save in cache...")
            self.save_in_cache()
            self.in_cache = True
        else:
            logging.debug("[TMDBSeason][tmdb_%s-S%s]Already in cache...", self.tmdb_id, self.season_obj['season_number'])

        if self.searched_season is not None and self.searched_season == self.season_obj['season_number']:
            #we need season details including episodes
            logging.debug("[TMDBSeason]Searched season...")
            if self.season_obj['id']!=0:
                diff = datetime.datetime.now() - self.season_obj['date_update']
                if diff.days > 1:
                    logging.debug("[TMDBSeason]Cache too old...")
                    self.fetch_tvshow_season_from_sources()
                    self.save_in_cache()
                else:
                    logging.debug("[TMDBSeason]Cache up to date...")
                    self.episodes = FunctionsCache.fetch_tvshow_episodes_data(tmdb_id=self.tmdb_id, lang=self.lang, season=self.searched_season)

                    if self.episodes is not None and len(self.episodes) == self.season_obj['episode_count']:
                        self.episodes_fetched_from_cache = True
                    else:
                        self.fetch_tvshow_season_from_sources()
                        self.save_in_cache()

                if self.episodes is not None:
                    t1 = time.time()

                    for episode in self.episodes:
                        # {u'still_path': None, u'name': u'Shilbottle', u'air_date': u'2014-03-01', u'overview': u'Stew shares his thoughts about social media and road signs along the A1 in Northumberland.',
                        # u'crew': [], u'guest_stars': [], u'vote_count': 0, u'season_number': 3, u'vote_average': 0.0, u'episode_number': 1, u'production_code': None, u'id': 976449},

                        if not episode.has_key('date_update'):
                            episode['date_update'] = datetime.datetime.now()
                        if not episode.has_key('date_creation'):
                            episode['date_creation'] = datetime.datetime.now()
                        # continue updating seasons and episodes data
                        e_ret = TMDBEpisode(tvshow_tmdb_id=self.tmdb_id, lang=self.lang, episode_obj=episode,
                                            date_update=episode['date_update'],
                                            date_creation=episode['date_creation'],
                                            fetched_from_cache=self.episodes_fetched_from_cache,
                                            session=self.session)

                        self.episodes_json.append(e_ret.build_json())
                    logging.info("[Profiling][TvShow][Episodes]Cache add/update took: %s", time.time() - t1)
        # elif self.searched_season is not None:
        #    logging.debug("[TMDBSeason]Not the searched season(%s vs %s)...", self.searched_season, self.season_obj['season_number'])


    # ---- Fetch TV Show data from cache
    def fetch_from_cache(self):

        self.cached = FunctionsCache.fetch_tvshow_season_data(self.tmdb_id, self.lang, season=self.season_obj['season_number'])

        if self.cached:
            self.in_cache = True


    # ---- Fetch seasons data from sources
    def fetch_tvshow_season_from_sources(self):

        result_tmdb = FunctionsTmdb.fetch_tvshow_season_data(tmdb_id=self.tmdb_id, season_number=self.season_obj['season_number'],
                                                             lang=self.lang)

        if result_tmdb:
            self.episode_count = len(result_tmdb['episodes'])
            self.episodes = result_tmdb['episodes']
            self.name = result_tmdb['name']
            self.season_id = result_tmdb['id']
            self.overview = result_tmdb['overview']
            self.poster_path = result_tmdb['poster_path']
            self.first_air_date = result_tmdb['air_date']

        self.fetched_from_sources = True

    def build_json(self):
        # we just want some data about the tv show
        if self.season_obj.has_key('id'):
            if self.season_obj['id']!=0:
                self.season_obj["exist"] = True
            else:
                self.season_obj["exist"] = False
            if self.searched_season is not None:
                self.season_obj["episodes"] = self.episodes_json
            if not isinstance(self.season_obj['date_update'], basestring):
                self.season_obj['date_update'] = (self.season_obj['date_update']).strftime('%Y-%m-%d %H:%M:%S')
            if not isinstance(self.season_obj['date_creation'], basestring):
                self.season_obj['date_creation'] = (self.season_obj['date_creation']).strftime('%Y-%m-%d %H:%M:%S')
            if self.season_obj.has_key('air_date') \
                    and self.season_obj['air_date'] is not None\
                    and not isinstance(self.season_obj['air_date'], basestring):
                self.season_obj['air_date'] = (self.season_obj['air_date']).strftime('%Y-%m-%d')
        else:
            self.season_obj["exist"] = False
        return self.season_obj

    # ----- Save TV Show in cache
    def save_in_cache(self):

        # If already in cache, update data
        if not self.season_obj.has_key('id'):
            if not self.in_cache:
                FunctionsCache.create_season(tmdb_id=self.tmdb_id,
                                             lang=self.lang,
                                             season=self.season_obj['season_number'],
                                             poster_path="",
                                             id=0,
                                             episode_count=0,
                                             air_date="12-31-2099", session=self.session)
        elif self.in_cache:
            FunctionsCache.update_season(tmdb_id=self.tmdb_id,
                                         lang=self.lang,
                                         season=self.season_obj['season_number'],
                                         poster_path=self.season_obj['poster_path'],
                                         id=self.season_obj['id'],
                                         episode_count=self.season_obj['episode_count'],
                                         air_date=self.season_obj['air_date'], session=self.session)

        # Otherwise create it
        elif self.fetched_from_sources:
            FunctionsCache.create_season(tmdb_id=self.tmdb_id,
                                         lang=self.lang,
                                         season=self.season_obj['season_number'],
                                         poster_path=self.season_obj['poster_path'],
                                         id=self.season_obj['id'],
                                         episode_count=self.season_obj['episode_count'],
                                         air_date=self.season_obj['air_date'], session=self.session)


class TMDBEpisode:
    def __init__(self, tvshow_tmdb_id, season_number=0, episode_number=0, episode_obj=None, lang=u'en',
                 fetched_from_cache=False, date_update="12-31-2099", date_creation="12-31-2099", session=None):

        self.serie_tmdb = tvshow_tmdb_id
        self.episode_number = episode_number
        self.season_number = season_number
        self.episode = episode_obj
        self.lang = lang
        self.tmdb_id = 0
        self.name = None
        self.poster_path = None
        self.air_date = None
        self.overview = None
        self.vote_average = 0
        self.last_update = None
        self.fetched_from_cache = fetched_from_cache
        self.in_cache = False
        self.session = session

        if not self.fetched_from_cache:
            self.fetch_from_cache()
            self.save_in_cache()

    # ---- Fetch TV Show data from cache
    def fetch_from_cache(self):

        cached = FunctionsCache.fetch_tvshow_episode_data(self.serie_tmdb, self.lang, self.episode['season_number'],
                                                               self.episode['episode_number'])

        if cached:
            logging.debug("[TMDBEpisode]Found in cache...")
            self.in_cache = True

    # ----- Save TV Show in cache
    def save_in_cache(self):
        # If already in cache, update data
        t1 = time.time()
        if self.in_cache:
            FunctionsCache.update_episode(serie_tmdb=self.serie_tmdb,
                                          name=self.episode['name'],
                                          lang=self.lang,
                                          season_number=self.episode['season_number'],
                                          still_path=self.episode['still_path'],
                                          episode_number=self.episode['episode_number'],
                                          id=self.episode['id'],
                                          air_date=self.episode['air_date'],
                                          overview=self.episode['overview'],
                                          vote_average=self.episode['vote_average'],
                                          session=self.session)

        # Otherwise create it
        else:
            FunctionsCache.create_episode(serie_tmdb=self.serie_tmdb,
                                          name=self.episode['name'],
                                          lang=self.lang,
                                          season_number=self.episode['season_number'],
                                          still_path=self.episode['still_path'],
                                          episode_number=self.episode['episode_number'],
                                          id=self.episode['id'],
                                          air_date=self.episode['air_date'],
                                          overview=self.episode['overview'],
                                          vote_average=self.episode['vote_average'],
                                          session=self.session)


    def build_json(self):

        if not self.episode:
            self.fetch_from_cache()
        if self.episode:
            if not isinstance(self.episode['date_update'], basestring):
                self.episode['date_update'] = (self.episode['date_update']).strftime('%Y-%m-%d %H:%M:%S')
            if not isinstance(self.episode['date_creation'], basestring):
                self.episode['date_creation'] = (self.episode['date_creation']).strftime('%Y-%m-%d %H:%M:%S')
            if self.episode.has_key('air_date') \
                    and self.episode['air_date'] is not None\
                    and not isinstance(self.episode['air_date'], basestring):
                self.episode['air_date'] = (self.episode['air_date']).strftime('%Y-%m-%d')
        else:
            self.episode["exist"] = False
        return self.episode
