# coding: utf-8

from slackclient import SlackClient
import Settings
import logging

sc = SlackClient(Settings.SLACK_TOKEN)

def send_message(channel,text,username,icon):

    try:
        sc.api_call(
        "chat.postMessage", channel=channel, text=text,
        username=username, icon_emoji=icon
        )
    except:
        logging.error("Can't post slack message")

