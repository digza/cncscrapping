# -*- coding: utf-8 -*-

from operator import itemgetter

import tmdbsimple as tmdb

tmdb.API_KEY = 'ebdc3b518d46ad26d5b10097cc13dd7c'


def get_similar_movies(tmdb_id):
    similar = tmdb.Movies(tmdb_id)
    sorted_list = sorted(similar.similar_movies()['results'],
                         key=itemgetter('popularity'),
                         reverse=True)
    id_list = []

    for elem in sorted_list:
        id_list.append(elem['id'])

    return id_list

print get_similar_movies(213)
