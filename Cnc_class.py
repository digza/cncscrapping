# -*- coding: utf-8 -*-

from sqlalchemy.engine.url import URL
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, Float, DateTime

# ----- Database connexion settings
DATABASE = {
    u'drivername': u'postgres',
    u'host': u'maxime.cpyzhnigbcgb.eu-west-1.rds.amazonaws.com',
    u'port': u'5432',
    u'username': u'maxime',
    u'password': u'maximesetup',
    u'database': u'maxime'
}

Base = declarative_base()
engine = create_engine(URL(**DATABASE), pool_size=0, max_overflow=-1)


class CNC_movies(Base):
    __tablename__ = 'cnc_movies'

    pkey = Column(String, primary_key=True)
    tmdb_id = Column(Integer)
    title = Column(String)
    country = Column(String)
    lang = Column(String)
    media_type = Column(String)
    provider = Column(String)
    url = Column(String)
    currency = Column(String)
    lowest_buy_price = Column(Float)
    lowest_rent_price = Column(Float)
    date_creation = Column(DateTime)
    date_update = Column(DateTime)
    is_premium = Column(Boolean)
    is_adult = Column(Boolean)

    def __init__(self, movie_data, providers):
        for key in movie_data:
            value = movie_data[key]
            setattr(self, key, value)
        for key in providers:
            if (key == 'lowest_rent_price' or key == 'lowest_buy_price') \
                    and providers[key] is not None:
                value = float(providers[key])
            else:
                value = providers[key]
            setattr(self, key, value)

        self.pkey = str(self.tmdb_id) \
                    + self.provider + '-' \
                    + self.country + '-' \
                    + self.lang

    def __repr__(self):
        return "<Cnc Release(title='%s', tmdb_id='%d', pkey='%s')>" %\
               self.title, self.tmdb_id, self.pkey

