for ((i = 1; i <= $1; i++)); do
    echo "--- Iteration #$i: $(date) ---"
    echo ""
    echo =============v2===============
    time python ../../FunctionsCncv2.py
    echo ""
    echo =============v3===============
    time python ../../FunctionsCnc.py
    echo ""
done  2>&1 | tee $2

../clean/cleanlog $2 > $3