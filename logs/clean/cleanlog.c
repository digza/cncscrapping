#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

char *get_next_line(int);

int main(int ac, char **av)
{
  int fd;
  int fd2;
  char *lign;

  if ((fd = open(av[1], O_RDONLY)) == -1)
    {
      printf("Open failed\n");
      return (-1);
    }
    while ((lign = get_next_line(fd)) != NULL)
      {
      if (lign && ((lign[0] == 'r' && lign[1] == 'e' && lign[2] == 'a' && lign[3] == 'l')|| lign[0] == '=' || (lign[0] == 's' && lign[1] == 'y' && lign[2] == 's') || lign[0] == 'u'))
	printf("%s\n", lign);
      if (lign && lign[0] == '-')
	printf("\n%s\n\n", lign);
    }
    return (0);
}
