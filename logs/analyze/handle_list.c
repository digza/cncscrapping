#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "analyze.h"

char *get_next_line(int);

void		init_list(t_beacon *beacon)
{
  beacon->head = NULL;
  beacon->tail = NULL;
  beacon->size = 0;
}

int		tail_add(t_beacon *beacon, int iter, char *data, int index, int fd)
{
  t_list	*new_elem;

  if ((new_elem = malloc(sizeof(*new_elem))) == NULL)
    return (-1);
  new_elem->iter = iter;
  ((index % 2 == 0) ? (new_elem->vers = 2) : (new_elem->vers = 3));
  new_elem->real = data;
  new_elem->user = get_next_line(fd);
  new_elem->sys = get_next_line(fd);
  if (beacon->head == NULL && beacon->tail == NULL)
    {
      new_elem->next = NULL;
      beacon->head = new_elem;
      beacon->tail = new_elem;
    }
  else
    {
      new_elem->next = NULL;
      beacon->tail->next = new_elem;
      beacon->tail = new_elem;
    }
  beacon->size++;
  return (0);
}
