#ifndef ANALYZE_H_
# define ANALYZE_H_

typedef struct	s_list
{
  int           iter;
  int           vers;
  char		*real;
  char		*user;
  char		*sys;
  int         seconds;
  struct s_list *next;
}		t_list;

typedef struct	s_beacon
{
  t_list	*head;
  t_list	*tail;
  int		size;
}		t_beacon;

#endif /* !MY_H_ */
