#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#undef BUFF_SIZE
# define BUFF_SIZE 1

char		*my_realloc_gnl(char *buffer, char next_char)
{
  int		index;
  char		*str;

  str = malloc(sizeof(char) * (strlen(buffer) + 3));
  if (str == NULL)
    return (NULL);
  index = 0;
  while (buffer[index])
    {
      str[index] = buffer[index];
      index += 1;
    }
  str[index] = next_char;
  str[index + 1] = '\0';
  free(buffer);
  return (str);
}

char		*get_next_line(const int fd)
{
  int		nread;
  static char	buffer[BUFF_SIZE + 1];
  char		*str;

  str = malloc(sizeof(char) * BUFF_SIZE);
  if (str == NULL)
    return (NULL);
  str[0] = '\0';
  while ((nread = read(fd, buffer, BUFF_SIZE)) > 0)
    {
      if (buffer[0] == '\n')
	return (str);
      str = my_realloc_gnl(str, buffer[0]);
    }
  if (str[0])
    return (str);
  return ((nread == 0) ? NULL : str);
}
